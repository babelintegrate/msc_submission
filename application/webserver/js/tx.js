"use strict";

function io_tx() {
    //Get the addressPattern
    var addressPatternBytes = [];
    var addressPattern = document.querySelector("#addressPattern").value;
    if (addressPattern === "") {
        alert("Address Pattern Cannot be Empty");
        return;
    }
    //Add a "/" if it doesn't start with one
    if (addressPattern.charAt(0) !== "/") {
        addressPattern = "/" + addressPattern;
    }

    //Convert to a stream of bytes with null terminations
    addressPatternBytes = turnToBytes(addressPattern, "s");

    //Work out which send button was pressed - what sort of a message is this going to be?
    switch (this.name) {
        case "none":
            sendNone();
            break;
        case "int":
            sendInt();
            break;
        case "float":
            sendFloat();
            break;
        case "string":
            sendString();
            break;
        case "blob":
            sendBlob();
            break;
        case "true":
            sendSingle("T");
            break;
        case "false":
            sendSingle("F");
            break;
        case "null":
            sendSingle("N");
            break;
        case "impulse":
            sendSingle("I");
            break;
        case "time":
            break;
        default:
            console.log("Unknown event from a button. " + this.name);
    }

    function sendNone() {
        send("", null);
    }

    //Make and send a new message of integer type
    function sendInt() {
        var data = parseInt(document.querySelector("#dataInt").value);
        if (isNaN(data))
            return;
        send("i", turnToBytes(data, "i"));
    }

    function sendFloat() {
        var data = parseFloat(document.querySelector("#dataFloat").value);
        if (isNaN(data))
            return;
        send("f", turnToBytes(data, "f"));
    }

    function sendString() {
        var data = document.querySelector("#dataString").value;
        send("s", turnToBytes(data, "s"));
    }

    function sendBlob() {
        var data = document.querySelector("#dataBlob").value;
        send("b", turnToBytes(data, "b"));
    }

    function sendSingle(type) {
        send(type);
    }

    /**
     * 
     * @param String tt
     * @param byte[] data
     */
    function send(tt, data) {
        var toSend = [];

        //Add the addressPattern to the message buffer
        for (var i in addressPatternBytes) {
            toSend.push(addressPatternBytes[i]);
        }

        if (tt !== null) {
            //Now add the TypeTag(s) to the message buffer
            var typeTagBytes = turnToBytes("," + tt, "s");

            //Make it a multiple of 4 bytes
            while (typeTagBytes.length % 4 !== 0) {
                typeTagBytes.push(0);
            }
            for (var i in typeTagBytes) {
                toSend.push(typeTagBytes[i]);
            }
        }

        if (data !== null) {
            //Add the data to the message buffer
            if (data) {
                for (var i in data) {
                    toSend.push(data[i]);
                }
            }
        }

        /*------------PRINTING-----------------*/
        /*var toPrint = "";
         for (var i in toSend) {
         toPrint += toSend[i].toString(16) + ",";
         }
         console.log("Sending: " + toPrint);
         
         var toPrint = "";
         for (var i in toSend) {
         toPrint += String.fromCharCode(toSend[i]) + ",";
         }
         console.log("Sending: " + toPrint);*/
        /*----------END-PRINTING------------*/

        socket.emit("message", toSend);
    }

    /**
     * Takes a var input and turns it in to an array of bytes
     * @param var data
     * @param String type
     * @returns byte[]
     */
    function turnToBytes(data, type) {
        switch (type) {
            case "i":
                return intToBytes(data);
            case "f":
                var view = new DataView(new ArrayBuffer(4));
                view.setFloat32(0, data);
                //console.log(view.getInt32(0).toString(16)); //bits of the 32 bit float
                return intToBytes(view.getInt32(0));
            case "s":
                var toReturn = [];
                for (var i in data) {
                    toReturn.push(data.charCodeAt(i));
                }
                toReturn.push(0);
                while ((toReturn.length % 4) !== 0) {
                    toReturn.push(0);
                }
                return toReturn;
            case "b":
                var toReturn = [];
                var b = data.split(",");
                var v = intToBytes(b.length);
                for (var i in v) {
                    toReturn.push(v[i]);
                }
                for (var i in b) {
                    v = intToBytes(parseInt(b[i], 10));
                    for (var i in v) {
                        toReturn.push(v[i]);
                    }
                }
                return toReturn;
            default:
        }
    }

    function intToBytes(num) {
        var arr = [
            (num & 0xff000000) >> 24,
            (num & 0x00ff0000) >> 16,
            (num & 0x0000ff00) >> 8,
            (num & 0x000000ff)
        ];
        return arr;
    }
}

