"use strict";

addEventListener("load", io_main);
var socket;

function io_main() {
    socket = io.connect(location.host);
    socket.on("connect", connected);
    socket.on("message", io_rx);
    //window.addEventListener("beforeunload", io_close);

    var submitButton = document.getElementsByClassName("submitButton");
    for (var i = 0; i < submitButton.length; i++) {
        submitButton[i].addEventListener("click", io_tx);
    }

    function connected() {
        console.log("WebSocket connected!");
    }

    function io_close() {
        socket.close();
    }
}
