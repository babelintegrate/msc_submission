"use strict";

addEventListener("load", commands);

function commands() {
    var socket = io.connect(location.host);

    var shutdown = document.getElementById("shutdown");
    var update = document.getElementById("update");
    var restart = document.getElementById("restart");
    var longrestart = document.getElementById("longrestart");
    var reboot = document.getElementById("reboot");
    var shutdownweb = document.getElementById("shutdownweb");

    shutdown.addEventListener("click", checkShutdown);
    update.addEventListener("click", checkUpdate);
    restart.addEventListener("click", checkRestart);
    longrestart.addEventListener("click", checkLongRestart);
    reboot.addEventListener("click", checkReboot);
    shutdownweb.addEventListener("click", checkShutdownWeb);

    function checkShutdownWeb() {
        var message = "Do you really want to shut down the webserver?";
        UIkit.modal.confirm(message, function () {
            socket.emit("control", "shutdown");
        }, [null, null]);
    }

    function checkShutdown() {
        var message = "Do you really want to shut the system down?";
        UIkit.modal.confirm(message, function () {
            send("/system/control/shutdown");
        }, [null, null]);
    }

    function checkRestart() {
        var message = "Do you really want to restart the software?";
        UIkit.modal.confirm(message, function () {
            send("/system/control/restart");
            var modal = UIkit.modal.blockUI("Rebooting..."); // modal.hide() to unblock
            setTimeout(function () {
                modal.hide();
            }, 5000);
        }, [null, null]);
    }

    function checkLongRestart() {
        var message = "Do you really want to restart the software?";
        UIkit.modal.confirm(message, function () {
            send("/system/control/longrestart");
            var modal = UIkit.modal.blockUI("Rebooting..."); // modal.hide() to unblock
            setTimeout(function () {
                modal.hide();
            }, 20000);
        }, [null, null]);
    }

    function checkReboot() {
        var message = "Do you really want to reboot the system?";
        UIkit.modal.confirm(message, function () {
            send("/system/control/reboot");
            var modal = UIkit.modal.blockUI("Rebooting..."); // modal.hide() to unblock
            setTimeout(function () {
                modal.hide();
            }, 30000);
        }, [null, null]);
    }

    function checkUpdate() {
        var message = "Do you really want to update the system?";
        UIkit.modal.confirm(message, function () {
            send("/system/control/update");
            var modal = UIkit.modal.blockUI("Updating..."); // modal.hide() to unblock
            setTimeout(function () {
                modal.hide();
            }, 6500);
        }, [null, null]);
    }

    function send(message) {
        var toSend = toBytes(message);
        toSend.push(",".charCodeAt(0));
        toSend.push("N".charCodeAt(0));
        toSend.push(0);
        toSend.push(0);
        socket.emit("message", toSend);
        //console.log(toSend);
    }

    function toBytes(data) {
        var toReturn = [];
        for (var i in data) {
            toReturn.push(data.charCodeAt(i));
        }
        toReturn.push(0);
        while ((toReturn.length % 4) !== 0) {
            toReturn.push(0);
        }
        return toReturn;
    }

    function tt() {
        var typeTagBytes = turnToBytes("," + "N", "s");
        //Make it a multiple of 4 bytes
        while (typeTagBytes.length % 4 !== 0) {
            typeTagBytes.push(0);
        }
        for (var i in typeTagBytes) {
            toSend.push(typeTagBytes[i]);
        }
    }
}