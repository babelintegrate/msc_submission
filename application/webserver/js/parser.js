"use strict";

/**
 * JavaScript OSC Parser
 */

var stuff = {buffer: [], ap: "", tt:"", d:[], bs:new BuildingState()};

function BuildingState() {
    this.gotAP = false;
    this.gotTT = false;
    this.gotData = false;

    this.reset = function reset() {
        this.gotAP = false;
        this.gotTT = false;
        this.gotData = false;
    };
}

function Parser() {

    function OSCMessage(ap, tt, data) {
        this.addressPattern = ap;
        this.typeTag = tt;
        this.data = data;
        this.parsedData = [];
        
        for(var j = 1; j < tt.length; j++){
            switch(tt[j]){
                case "i":
                    var n = [4];
                    for(var i = 0; i < 4; i++){
                        n[i] = this.data.shift();
                    }
                    this.parsedData.push(toInt(n));
                    break;
                case "f":
                    var n = [4];
                    for(var i = 0; i < 4; i++){
                        n[i] = this.data.shift();
                    }
                    this.parsedData.push(toFloat(n));
                    break;
                case "s":
                    var length = indexOfNull(this.data);
                    var chars = [length];
                    for(var i = 0; i < length; i++){
                        chars[i] = String.fromCharCode(this.data.shift());
                    }
                    if(this.data.shift() !== 0){ //read in null
                        console.log("Parser.OSCMessage just threw away real data");
                    }
                    length++;
                    var read = length;
                    while(read % 4 !== 0){
                        if(this.data.shift() !== 0){
                            console.log("Parser.OSCMessage just threw away real data");
                        }
                        read++;
                    }
                    var s = chars.join("");
                    //console.log("string:" + s + " data: " + this.data);
                    this.parsedData.push(chars.join(""));
                    break;
                case "b":
                    var n = [4];
                    for (var j = 0; j < 4; j++) {
                        n[j] = this.data.shift();
                    }
                    var length = toInt(n);
                    for(var i = 0; i < length; i++){
                        this.parsedData.push(this.data.shift());           
                    }
                    break;
                case "T":
                    break;
                case "F":
                    break;
                case "N":
                    break;
                case "I":
                    break;
            }
        }
        
        function toInt(n) {
            var number = n[0] << 24;
            number += (n[1] & 0x000000FF) << 16;
            number += (n[2] & 0x000000FF) << 8;
            number += (n[3] & 0x000000FF);
            return number;
        }
        
        function toFloat(n) {
            var view = new DataView(new ArrayBuffer(4));
            view.setInt32(0, toInt(n));
            return view.getFloat32(0);
        }
        
        function indexOfNull(buffer) {
            return buffer.indexOf(0);
        }
    }

    this.parse = function parse(data) {
        if (data !== null) {
            for (var i in data) {
                stuff.buffer.push(data[i]);
            }
        }
        //console.log("buffer: " + stuff.buffer);

        if (readyToGetAP(stuff)) {
            //console.log("ap: " + stuff.ap + " buffer: " + stuff.buffer);
            stuff.ap = getAP(stuff);
            stuff.bs.gotAP = true;
        }

        if (readyToGetTT(stuff)) {
            stuff.tt = getTT(stuff);
            //console.log("tt: " + stuff.tt + " buffer: " + stuff.buffer);
            stuff.bs.gotTT = true;
        }

        if (readyToGetData(stuff)) {
            stuff.d = getData(stuff);
            if (stuff.d !== null) {
                //console.log("d: " + stuff.d + " buffer: " + stuff.buffer);
                stuff.bs.gotData = true;
            }
        }

        if (gotFullMessage(stuff)) {
            //console.log("Message: " + stuff.ap + stuff.tt + " " + stuff.d);
            stuff.bs.reset();
            return new OSCMessage(stuff.ap, stuff.tt, stuff.d);
        }
        return null;//if there is no full message yet
    };

    function readyToGetAP(stuff) {
        return !stuff.bs.gotAP && !stuff.bs.gotTT && !stuff.bs.gotData && indexOfComma(stuff.buffer) > 0;
    }

    function readyToGetTT(stuff) {
        return stuff.bs.gotAP && !stuff.bs.gotTT && !stuff.bs.gotData && indexOfComma(stuff.buffer) === 0 && indexOfNull(stuff.buffer) > 0 && enoughForTT(stuff);
    }

    function readyToGetData(stuff) {
        return stuff.bs.gotAP && stuff.bs.gotTT && !stuff.bs.gotData && enoughData(stuff);
    }

    function gotFullMessage(stuff) {
        return stuff.bs.gotAP && stuff.bs.gotTT && stuff.bs.gotData;
    }

    function getAP(stuff) {
        var builder = "";
        while (stuff.buffer[0] !== 0x2C) {
            var i = stuff.buffer.shift();
            var c = String.fromCharCode(i);
            if (c !== '\0') {
                builder = builder + c;
            }
        }
        return builder;
    }

    function getTT(stuff) {
        var builder = "";
        var bytesRead = 0;

        while (stuff.buffer[0] !== 0) {
            var i = stuff.buffer.shift();
            var c = String.fromCharCode(i);
            builder = builder + c;
            bytesRead++;
        }
        while (bytesRead % 4 !== 0) {
            if (stuff.buffer.shift() !== 0) {
                console.log("OSC.Parser.getTT just threw away real data.");
            }
            bytesRead++;
        }
        return builder;
    }

    function getData(stuff) {
        var count = calculateHowMuchData(stuff);
        if (count > stuff.buffer.length) {
            //console.log("parser.getData Dont have enough data: " + stuff.buffer);
        } else {
            var a = [count];
            for (var j = 0; j < count; j++) {
                a[j] = stuff.buffer.shift();
            }
            return a;
        }
        return null;
    };

    function calculateHowMuchData(stuff) {
        var count = 0;
        //for each TT
        for (var i = 0; i < stuff.tt.length; i++) {
            switch (stuff.tt[i]) {
                case 'f':
                case 'i':
                    count += 4;
                    break;
                case 's':
                    for (var j = count; stuff.buffer[j] !== 0 && j < stuff.buffer.length; j++) {
                        count += 1;
                    }
                    count++; //to account for the \0
                    while (count % 4 !== 0) {
                        count++;
                    }
                    break;
                case 'b':
                    var n = [4];
                    for (var j = 0; j < 4; j++) {
                        n[j] = stuff.buffer[j + count];
                    }
                    var numBytes = (toInt(n) + 4);
                    count += numBytes;
                    break;
                default:
            }
        }
        return count;
    };

    function enoughForTT(stuff) {
        var n = indexOfNull(stuff.buffer);
        var r = (4 - (n % 4));
        return n > 0 && stuff.buffer.length >= (n + r);
    };

    function enoughData(stuff) {
        var index = 0;
        var s = stuff.buffer.length;
        var numberOfData = stuff.tt.length;
        for (var i = 1; i < numberOfData; i++) {
            switch (stuff.tt[i]) {
                case ',':
                    break;
                case 'i':
                    if (s < (index + 4)) {
                        return false;
                    }
                    index += 4;
                    break;
                case 'f':
                    if (s < (index + 4)) {
                        return false;
                    }
                    index += 4;
                    break;
                case 's':
                    var foundNull = false;
                    while (!foundNull && index < s) {
                        if (stuff.buffer[index] === 0) {
                            foundNull = true;
                        }
                        index++;
                    }
                    //check packed with enough nulls
                    while (index % 4 !== 0 && index < s) {
                        if (stuff.buffer[index] !== 0) {
                            return false;
                        }
                        index++;
                    }
                    if ((index > s)) {
                        return false;
                    }
                    break;
                case 'b':
                    var n = [4];
                    for (var j = 0; j < 4; j++) {
                        n[j] = stuff.buffer[j + index];
                    }
                    var numBytes = (toInt(n) + 4);
                    index += numBytes;
                    if(index > s){
                        return false;
                    }
                    break;
                default:
            }
        }
        return true;
    }

    function indexOfComma(buffer) {
        return buffer.indexOf(0x2C);
    } 

    function indexOfNull(buffer) {
        return buffer.indexOf(0);
    }

    function toInt(n) {
        var number = n[0] << 24;
        number += (n[1] & 0x000000FF) << 16;
        number += (n[2] & 0x000000FF) << 8;
        number += (n[3] & 0x000000FF);
        return number;
    }
    
    
    //CLEANER
    
    this.forCleaner = {size: 0, parse: this.parse};
    this.cleaner = window.setInterval(cleanBuffer.bind(null, this.forCleaner), 300);

    function cleanBuffer(forCleaner) {
        var size = stuff.buffer.length;
        //console.log("size: " + size + " oldSize: " + forCleaner.size + " buffer: " + stuff.buffer);
        if (size > 0 && size === forCleaner.size) {
            //console.log("Cleaning buffer: " + stuff.buffer);
            while (stuff.buffer.length > 0 && stuff.buffer[0] !== 0x2F) {
                stuff.buffer.shift();
            }
            stuff.bs.reset();
            size = stuff.buffer.length;
            forCleaner.parse(null);
        }
        forCleaner.size = size;
    }

}