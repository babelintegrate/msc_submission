"use strict";

var rxBuffer = [];
var rxMessageState = {apComplete: false, ttComplete: false, bytesReadFromtt: 0, dComplete: false, numDRead: 0};
var rxMessage = {ap: "", tt: "", d: []};
var cleaner = window.setInterval(cleanBuffer, 300);

var bufSizeWatcher = 0;
function cleanBuffer() {
    var size = rxBuffer.length;
    if (size > 0 && size === bufSizeWatcher) {
        while (rxBuffer.length > 0 && rxBuffer[0] !== 0x2F) {
            rxBuffer.shift();
        }
        reset();
        size = rxBuffer.length;
        io_rx(null);
    }

    bufSizeWatcher = size;
}

var parser = new Parser();
function io_rx(data) {    
    var returned = parser.parse(data);
    if(returned !== null){
        rxMessage.ap = returned.addressPattern;
        rxMessage.tt = returned.typeTag;
        rxMessage.d = returned.parsedData;
        messageToPage();
    }

    //inner methods

    function checkForFullAddressPattern() {
        if (rxMessageState.apComplete === false && indexOfNull() > 0) { //We have got all of the addressPattern
            while (rxBuffer[0] !== 0) { //Read in AddressPattern
                rxMessage.ap = rxMessage.ap + String.fromCharCode(rxBuffer.shift());
            }
            while (rxBuffer[0] === 0) {
                rxBuffer.shift();
            }
            rxMessageState.apComplete = true;
        }
    }

    function checkForTypeTag() {
        console.log("Checking for TT");
        if (rxMessageState.apComplete === true && rxMessageState.ttComplete === false) {
            if (indexOfComma() === 0 && indexOfNull() > 0) {  //We are now at the TypeTags and have full list of TTs
                var t = "";
                while (rxBuffer[0] !== 0) {
                    t = t + String.fromCharCode(rxBuffer.shift());
                    rxMessageState.bytesReadFromtt++;
                }
                rxMessage.tt = t;
            }
            if (rxBuffer.length >= (rxMessageState.bytesReadFromtt % 4)) { //remove only the right amount of nulls
                while (rxMessageState.bytesReadFromtt % 4 !== 0) {
                    rxBuffer.shift();
                    rxMessageState.bytesReadFromtt++;
                }
                if (rxMessage.tt.indexOf("i") < 0 &&
                        rxMessage.tt.indexOf("f") < 0 &&
                        rxMessage.tt.indexOf("s") < 0 &&
                        rxMessage.tt.indexOf("b") < 0) {
                    rxMessageState.dComplete = true;
                }
                console.log("TypeTag: " + rxMessage.tt);
                rxMessageState.ttComplete = true;
            }
        }
    }

    function getData() { //WHAT IF SOME ARE T F N I?
        if (!rxMessageState.dComplete && indexOfComma() < 0 && rxMessage.ap !== "" && rxMessage.tt.length > 1) { //Now to read the bytes
            console.log("Reading: " + rxMessage.tt[rxMessageState.numDRead + 1]);
            switch (rxMessage.tt[rxMessageState.numDRead + 1]) {
                case "i":
                    if (enougBytesAvailable("i")) {
                        var v = read32bits();
                        rxMessage.d.push(toIntFromBytes(v));
                        rxMessageState.numDRead++;
                    }
                    break;
                case "f":
                    if (enougBytesAvailable("f")) {
                        var v = read32bits();
                        rxMessage.d.push(toFloatFromBytes(v));
                        rxMessageState.numDRead++;
                    }
                    break;
                case "s":
                    if (enougBytesAvailable("s")) {
                        var n = indexOfNull();
                        var r = (4 - (n % 4));
                        var numberToRead = n + r;
                        var string = "";
                        for (var i = 0; i < numberToRead; i++) {
                            var get = rxBuffer.shift();
                            if (get !== 0) {
                                string += String.fromCharCode(get);
                            }
                        }
                        rxMessage.d.push(string);
                        rxMessageState.numDRead++;
                    }
                    break;
                case "b":
                    if (enougBytesAvailable("b")) {
                        var length = toIntFromBytes(read32bits());
                        for (var i = 0; i < length; i++) {
                            rxMessage.d.push(toIntFromBytes(read32bits()));
                        }
                        rxMessageState.numDRead++;
                    }
                    break;
                default:
            }

            if (rxMessageState.numDRead === (rxMessage.tt.length - 1)) {
                rxMessageState.dComplete = true;
            }
        }
    }

    function read32bits() {
        var v = [];
        v.push(rxBuffer.shift());
        v.push(rxBuffer.shift());
        v.push(rxBuffer.shift());
        v.push(rxBuffer.shift());
        return v;
    }

    function peak32bits(offset) {
        var v = [];
        v.push(rxBuffer[0 + offset]);
        v.push(rxBuffer[1 + offset]);
        v.push(rxBuffer[2 + offset]);
        v.push(rxBuffer[3 + offset]);
        return v;
    }

    function enougBytesAvailable(s) {
        switch (s) {
            case "i":
                if (rxBuffer.length >= 4) {
                    return true;
                }
                return false;
            case "f":
                if (rxBuffer.length >= 4) {
                    return true;
                }
                return false;
            case "s":
                var n = indexOfNull();
                var r = (4 - (n % 4));
                if (n > 0 && rxBuffer.length >= (n + r)) {
                    return true;
                }
                return false;
            case "b":
                if (rxBuffer.length >= 4) {
                    var number = toIntFromBytes(peak32bits(0));
                    //console.log("number: " + number + " available: " + rxBuffer.length + " buffer: " + rxBuffer);
                    if (rxBuffer.length >= ((number + 1) * 4)) {
                        return true;
                    }
                }
                return false;
            default:
                return false;
        }
    }

    function toIntFromBytes(bytes) {
        var number = bytes[0] << 24;
        number += (bytes[1] & 0x000000FF) << 16;
        number += (bytes[2] & 0x000000FF) << 8;
        number += (bytes[3] & 0x000000FF);
        return number;
    }

    function toFloatFromBytes(bytes) {
        var number = bytes[0] << 24;
        var view = new DataView(new ArrayBuffer(4));
        for (var i in bytes) {
            view.setInt8(i, bytes[i]);
        }
        return view.getFloat32(0);
    }

    function messageToPage() {
        var message = rxMessage.ap + rxMessage.tt + " " + rxMessage.d.toString();
        stringToPage(message);
    }

    function stringToPage(string) {
        var c = document.querySelector("#console");
        var p = document.createElement("p");
        p.innerHTML = string;
        c.appendChild(p);
        c.scrollTop = c.scrollHeight;
        while(c.childElementCount > 20){
            c.removeChild(c.firstChild);
        }
    }

    function indexOfComma() {
        return rxBuffer.indexOf(0x2C);
    }

    function indexOfNull() {
        return rxBuffer.indexOf(0);
    }

    function print(prefix, d) {
        //------------PRINTING-----------------
        var toPrint = "";
        for (var i in d) {
            toPrint += d[i].toString(16) + ",";
        }
        console.log(prefix + toPrint);

        var toPrint = "";
        for (var i in d) {
            toPrint += String.fromCharCode(d[i]) + ",";
        }
        console.log(prefix + toPrint);
        console.log();
        //----------END-PRINTING------------
    }
}

function reset() {
    rxMessageState = {apComplete: false, ttComplete: false, bytesReadFromtt: 0, dComplete: false, numDRead: 0};
    rxMessage = {ap: "", tt: "", d: []};
}   