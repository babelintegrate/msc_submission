"use strict";

addEventListener("load", io_main);
var socket;

function io_main() {
    socket = io.connect(location.host);
    socket.on("connect", connected);
    socket.on("config", gotconfig);
    getconfig();
    //window.addEventListener("beforeunload", io_close);

    function gotconfig(data) {
        var textbox = document.getElementById("textbox");
        textbox.value = data;
    }

    function send() {
        var textbox = document.getElementById("textbox");
        socket.emit("config", textbox.value);
        alert("File sent.");
    }

    var submitButton = document.getElementById("send");
    submitButton.addEventListener("click", send);

    var refreshButton = document.getElementById("refresh");
    refreshButton.addEventListener("click", getconfig);

    function getconfig() {
        socket.emit("getconfig", null);
    }

    function connected() {
        console.log("WebSocket connected!");
    }

    function io_close() {
        socket.close();
    }
}