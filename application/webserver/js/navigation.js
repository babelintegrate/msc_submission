"use strict";

addEventListener("load", writenav);

function writenav() {

    var page = [
        {location: "/index.html", name: "Babel"},
        {location: "/console.html", name: "Console"}
        /*{location: "/config.html", name: "Configuration"}*/
    ];
    var content = document.getElementById("content");

    var container = document.getElementById("maincontainer");
    container.removeChild(content);
    //container.className = "uk-container uk-width-large-* uk-container-center uk-margin-top uk-margin-bottom";
    container.className = "uk-container uk-width-1-1 uk-container-center uk-margin-top uk-margin-bottom";

    var weAreHere = window.location.pathname;

    container.appendChild(createTopNav());
    container.appendChild(createOffScreenNav());
    container.appendChild(content);

    function createLink(page, text) {
        var link = document.createElement("a");
        link.href = page;
        link.innerHTML = text;
        return link;
    }

    function navItem(page, text, defaultClass) {
        var l = document.createElement("li");
        var a = createLink(page, text);
        l.appendChild(a);
        l.className = defaultClass;
        if (page === weAreHere) {
            l.className = "uk-active";
        }
        return l;
    }
    
    function navButton(text, defaultClass, id){
        var l = document.createElement("li");
        /*var a = createLink("", text);
        a.id = id;
        l.appendChild(a);
        l.className = defaultClass;*/
        
        var b = document.createElement("button");
        b.id = id;
        b.className = "uk-button uk-width-1-1 uk-button-link";
        b.type = "button";
        b.innerHTML = text;
        l.appendChild(b);
        //<button id="shutdown" class="uk-button uk-button-primary" type="button">Shutdown</button>

        return l;
    }

    function createTopNav() {
        var nav = document.createElement("nav");
        nav.className = "uk-navbar";

        var list = document.createElement("ul");
        list.className = "uk-navbar-nav";
        
        var offcanvasButton = createLink("#offcanvas-nav", "");
        offcanvasButton.className = "uk-navbar-toggle uk-visible-small";
        offcanvasButton.setAttribute('data-uk-offcanvas', "");
        list.appendChild(offcanvasButton);

        var brand = createLink(page[0].location, page[0].name);
        if(weAreHere === page[0].location){
            brand.className = "uk-navbar-brand";
        } else {
            brand.className = "uk-navbar-brand uk-hidden-small";
        }

        list.appendChild(brand);
        for (var i = 1; i < page.length; i++) {
            list.appendChild(navItem(page[i].location, page[i].name, "uk-hidden-small"));
        }
        nav.appendChild(list);
        
        var flipNav = document.createElement("div");
        flipNav.className = "uk-navbar-content uk-navbar-flip";
        
        var flipContents = document.createElement("ul");
        flipContents.className = "uk-navbar-nav";
        flipContents.appendChild(addCommandButtonsDropDown());
        flipNav.appendChild(flipContents);
        nav.appendChild(flipNav);

        return nav;
    }
    
    function addCommandButtonsDropDown(){
        var theMenu = document.createElement("li");
        theMenu.className = "uk-parent";
        theMenu.setAttribute('data-uk-dropdown', "");
        
        var parentItem = document.createElement("a");
        parentItem.href = "";
        parentItem.innerHTML = "System Controls <i class='uk-icon-caret-down'></i>";
        theMenu.appendChild(parentItem);
        
        var dropdownList = document.createElement("div");
        dropdownList.className = "uk-dropdown uk-dropdown-navbar";
        
        var dropdownContents = document.createElement("ul");
        dropdownContents.className = "uk-nav uk-nav-navbar";
        
        var item1 = navButton("Restart Software", "uk-button-secondary", "restart");
        var item2 = navButton("Restart Software (long)", "uk-button-secondary", "longrestart");
        var item3 = navButton("Shutdown Machine", "uk-button-secondary", "shutdown");
        var item4 = navButton("Reboot Machine", "uk-button-primary", "reboot");
        var item5 = navButton("Update", "uk-button-primary", "update");
        var item6 = navButton("Shutdown Webserver", "uk-button-primary", "shutdownweb");
        dropdownContents.appendChild(item1);
        dropdownContents.appendChild(item2);
        dropdownContents.appendChild(item3);
        dropdownContents.appendChild(item4);
        dropdownContents.appendChild(item5);
        dropdownContents.appendChild(item6);
        
        dropdownList.appendChild(dropdownContents);
        theMenu.appendChild(dropdownList);
        
        return theMenu;
    }

    function createOffScreenNav() {
        var offScreenNav = document.createElement("div");
        offScreenNav.className = "uk-offcanvas";
        offScreenNav.id = "offcanvas-nav";

        var bar = document.createElement("div");
        bar.className = "uk-offcanvas-bar";

        var list = document.createElement("ul");
        list.className = "uk-nav uk-nav-offcanvas uk-nav-parent-icon";
        list.setAttribute('data-uk-nav', "");

        for (var i = 0; i < page.length; i++) {
            list.appendChild(navItem(page[i].location, page[i].name, "uk-visible-small"));
        }
        
        var divider = document.createElement("li");
        divider.className="uk-nav-divider";
        list.appendChild(divider);
        
        bar.appendChild(list);
        offScreenNav.appendChild(bar);

        return offScreenNav;
    }
}