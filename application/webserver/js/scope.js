"use strict";

var tester = new Test(1,2);
console.log("Adder: " + tester.add());
console.log("Sub: " + tester.sub());
tester.reset();
console.log("Adder: " + tester.add());
console.log("Sub: " + tester.sub());


function Test(initA, initB){
    this.a = initA;
    this.b = initB;
    
    this.add = function adder(){
        return this.a + this.b;
    };
    
    this.sub = function sub(){
        return this.a - this.b;
    };
    
    this.reset = function reset(){
        this.a = 0;
        this.b = 0;
    };
}

