"use strict";

var SOCKET_IO = require("socket.io");
var babelConnection = require("./tcpClient.js");
var FS = require('fs');
var master = require('./../server.js');

module.exports.init = _init;
module.exports.shutdown = _shutdown;
var io;

function _init(service) {
    //https://nodesource.com/blog/understanding-socketio/
    io = new SOCKET_IO(service);
    io.on("connection", handleClient);
    babelConnection.init(receive, "localhost", "2323");
}

var socket;
function handleClient(s) {
    // we've got a client connection
    //console.log("WEBSERVER: Got a connection");
    socket = s;
    socket.on("message", messageHandle);
    socket.on("getconfig", getconfig);
    socket.on("config", saveconfig);
    socket.on("control", systemControl);
}

function systemControl(message) {
    if(message === "shutdown"){
        console.log("calling to shutdown webserver");
        process.exit();
        master.masterShutdown();
    }
}

function getconfig() {
    //console.log("Send Config");
    FS.readFile('../config.babel', 'utf8', readFile);
}

var shutdownCommand = [0xFF, 0x00, 0xFF, 0x00, 0xFF];
function saveconfig(data){
    //console.log(data);
    if(data === shutdownCommand){
        systemControl("shutdown");
    }
    FS.writeFile('../config.babel', data, 'utf8', error);
}

function error(err){
    if (err) {
        return console.log(err);
    }
}

function readFile(err, data) {
    if (err) {
        return console.log(err);
    }
    //console.log(data);
    socket.emit("config", data);
}

function messageHandle(message) {
    //console.log("WEBSERVER: io.js - message: " + message);
    babelConnection.send(message);
}

//var buffer = [];
function receive(data) {
    //var theData = Array.from(data);
    //console.log("WEBSERVER: Rx (From TCP): " + data);
    io.sockets.emit("message", data); //Sends to all sockets
}

function _shutdown() {
    babelConnection.shutdown();
    io.close();
}
