   "use strict";

   //Imports

   //Exports
   module.exports.redirect = _redirect;

   var OK = 200, NotFound = 404, BadType = 415, Error = 500, Redirect = 308;

   function _redirect(request, response) {
      var url = request.url;
      //console.log("host:" + request.headers.host);
      var host = request.headers.host.split(":");
      if(host[1] === "8080"){
          response.writeHead(Redirect, {Location: "https://"+host[0]+":8443" + url});
      } else {
          response.writeHead(Redirect, {Location: "https://"+host[0] + url});
      }
      response.end();
   }
