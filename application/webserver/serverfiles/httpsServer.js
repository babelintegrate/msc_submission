"use strict";
/**
 * cert and key generated with "openssl req -x509 -sha512 -newkey rsa:2048 -keyout key.pem -out cert.pem -days 365"
 */

//Imports
var FS = require("fs");
var HTTP = require("https");
var URL_VALIDATION = require("./urlValidation.js");
var IO = require("./io.js");

//Exports
module.exports.start = _start;
module.exports.service = _start.service;
module.exports.shutdown = _shutdown;

//Code
var service;

function _start(port, errorFunction) {
    var credentials = {
        key: FS.readFileSync("./serverfiles/utilities/key.pem"),
        cert: FS.readFileSync("./serverfiles/utilities/cert.pem"),
        passphrase: "verysecurepassword"
    };

    service = HTTP.createServer(credentials, URL_VALIDATION.validate);
    service.on('listening', function () {
        console.log('Webserver is running on port ' + port);
    });
    service.on('error', errorFunction);
    IO.init(service);


    //service.listen(port, "localhost");
    service.listen(port, "");

    //Listens for the kill command
    process.on("SIGTERM", _shutdown);
    //Listens for the Ctrl-C command
    process.on("SIGINT", _shutdown);
}

function _shutdown() {
    console.log("WEBSERVER: Server connection closed.");
    IO.shutdown();
    service.close();
    process.exit();
}