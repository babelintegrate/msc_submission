"use strict";

//Imports
var HTTP = require("http");
var REDIRECTOR = require("./httpToHttps.js");

//Exports
module.exports.start = _start;
module.exports.shutdown = _shutdown;

//Code
var service;

// Provide a service to localhost only.
function _start(port, errorFunction) {
    service = HTTP.createServer(REDIRECTOR.redirect);
    service.on('error', errorFunction);
    service.on('listening', function () {
        console.log('Webserver is running on port ' + port);
    });
    service.listen(port, "");
}

function _shutdown(){
    service.close();
    process.exit();
}
