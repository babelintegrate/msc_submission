"use strict";

var NET = require('net');

module.exports.init = _init;
module.exports.send = _send;
module.exports.shutdown = _shutdown;

var server;
var options;
var rx;

function _init(receiveHandler, host, port) {
    rx = receiveHandler;
    options = {host: host, port: port};
    _connect();
}

function _connect() {
    server = NET.connect(options, connected);
    server.on('error', error);
}

function connected() {
    server.on('data', addToBuffer);
    server.on('close', closed);
}

function _send(data) {
    var tosend = [];

    for (var i = 0; i < data.length; ++i) {
        tosend.push(data[i]);
    }
    //console.log("WEBSERVER: Tx (From TCP): " + tosend);
    server.write(new Buffer(tosend));
}

function toBytesInt32(num) {
    var arr = [
        (num & 0xff000000) >> 24,
        (num & 0x00ff0000) >> 16,
        (num & 0x0000ff00) >> 8,
        (num & 0x000000ff)
    ];
    return arr;
}

function error(ex) {
    console.log("WEBSERVER: " + ex);
    process.exit();
    //setTimeout(_connect, 2000);
}

function closed(data) {
    console.log("WEBSERVER: Lost connection.");
    process.exit();
    //setTimeout(_connect, 2000);
}

var buffer = [];
function addToBuffer(data) {
    for (var v = 0; v < data.length; v++) {
        buffer.push(data[v]);
    }
    rx(buffer);
    buffer = [];
}
function _shutdown() {
    if (server) {
        server.destroy();
    }
    console.log("WEBSERVER: Link to Babel closed");
}
