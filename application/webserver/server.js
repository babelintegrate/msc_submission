"use strict";

//http://serverfault.com/questions/112795/how-can-i-run-a-server-on-linux-on-port-80-as-a-normal-user
// For using port 80 and 403

//Imports
var httpServer = require('./serverfiles/httpServer.js');
var httpsServer = require('./serverfiles/httpsServer.js');

module.exports.mastershutdown = mastershutdown;


//Code
console.log("Booting WebServer");
process.title = "BabelWebServer";

function terminalError(){
    console.log("WEBSERVER CANNOT START");
}

function httpPermissionsError(){
    httpServer.start(8080, terminalError);
}

function httpsPermissionsError(){
    httpsServer.start(8443, terminalError);
}

try{
    httpServer.start(80, httpPermissionsError);
    httpsServer.start(443, httpsPermissionsError);
} catch (err){
    console.log("WEBSERVER ERROR: " + err.message);
}

function mastershutdown(){
    httpServer.shutdown();
    httpsServer.shutdown();
}
