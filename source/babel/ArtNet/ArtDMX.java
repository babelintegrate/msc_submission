package babel.ArtNet;

import babel.ArtNet.Enums.OppCode;
import babel.OSC.OSCMessage;
import babel.OSC.OSCMessageBuilder;

/**
 * For generating an ArtDMX message
 * @author T R Bland
 */
public class ArtDMX {

    private final String id = "Art-Net\0";
    private final int oppCode = OppCode.OUTPUT.value();
    private final int protVer;
    private final byte sequence;
    private final byte physical;
    private final int universe;
    private final byte data[];

    private final int length;

    /**
     *
     * @param d
     */
    public ArtDMX(byte d[]) {
        protVer = (d[10] << 8) + d[11];
        sequence = d[12];
        physical = d[13];
        universe = ((d[14] & 0x7F) << 8) + d[15];
        length = (d[16] << 8) + d[17];
        data = new byte[length];
        for (int i = 0; i < length; i++) {
            data[i] = d[18 + i];
        }
        //System.out.println("ArtDMX: Universe: " + universe + " length: " + length + " data: " + Arrays.toString(data));
    }

    /**
     *
     * @param source
     * @return
     */
    public OSCMessage toOSC(Object source) {
        OSCMessageBuilder m = new OSCMessageBuilder(source, "/dmx/" + universe);
        m.add(data);
        OSCMessage message = m.finalise();
        return message;
    }

    OSCMessage[] toOSCindividual(Object source) {
        OSCMessage[] m = new OSCMessage[length];
        for (int i = 0; i < length; i++) {
            m[i] = new OSCMessage(source, "/dmx/" + universe + "/" + i, data[i] & 0xFF);
        }
        return m;
    }

    byte[] data() {
        return data;
    }

    OSCMessage toOSCindividual(ArtNetRx source, int i) {
        return new OSCMessage(source, "/dmx/" + universe + "/" + i, data[i] & 0xFF);
    }
}
