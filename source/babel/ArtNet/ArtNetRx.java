package babel.ArtNet;

import babel.ArtNet.Enums.OppCode;
import static babel.ArtNet.Enums.OppCode.*;
import babel.Hub.Hub;
import babel.NetTools.SystemInfo;
import babel.NetTools.UDPserver;
import babel.OSC.OSCMessage;
import babel.TMS.TypelessTMS;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * For implementing an Art-Net Node. ONLY PROVIDES RX FUNCTIONALITY
 * @author T R Bland
 */
public class ArtNetRx extends Thread {

    private final AtomicBoolean running = new AtomicBoolean(false);
    private final Hub hub = Hub.get();
    private final UDPserver udp;
    private final TypelessTMS rx;
    //The UDP port used as both source and destination is 0x1936
    private static final int UDP_PORT_NUMBER = 0x1936;
    //Products implementing Art-Net should default to the Primary IP address of 2.?.?.?. - NOT POSSIBLE IN THIS SITUATION
    //need a timer that is reset every time an art poll is recieved. If timer goes over 3.5seconds then controller is disconnected - send OSC error.

    /**
     *
     * @throws Exception
     */
    public ArtNetRx() throws Exception {
        super("ArtNetRx");
        try {
            udp = new UDPserver(UDP_PORT_NUMBER, "txrx");
            rx = udp.messages;
        } catch (SocketException ex) {
            throw new Exception("ArtNetRx can't create UDP port");
        }
    }

    @Override
    public void run() {
        udp.start();
        running.set(true);
        while (running.get()) {
            checkRx();
            sleep();
        }
        udp.shutdown();
    }

    private void sleep() {
        try {
            Thread.sleep(0,1);
        } catch (InterruptedException e) {
            System.out.println("Exception in Pinger" + e.getLocalizedMessage());
        }
    }

    private void checkRx() {
        if (rx.check()) {
            try {
                DatagramPacket p = (DatagramPacket) rx.get();
                if (isArtNet(p.getData())) {
                    decodePacket(p);
                }
            } catch (java.lang.NullPointerException negEx) {
                System.err.println("ArtnetRx: " + negEx.toString());
                System.err.println("\t" + negEx.getLocalizedMessage());
                System.err.println("\t" + negEx.getMessage());
                negEx.printStackTrace();
            } catch (Exception ex) {
                System.err.println("ArtnetRx: " + ex.toString());
                System.err.println("\t" + ex.getLocalizedMessage());
                System.err.println("\t" + ex.getMessage());
            }
        }
    }

    private boolean isArtNet(byte[] data) {
        if (data.length < 14) {
            return false;
        }
        byte[] refId = {'A', 'r', 't', '-', 'N', 'e', 't', '\0'};
        byte[] id = Arrays.copyOfRange(data, 0, 8);
        return Arrays.equals(id, refId);
    }

    private final ArrayList<ConnectedController> connectedControllers = new ArrayList<>();

    private void decodePacket(DatagramPacket p) {
        switch (decodeOppCode(p.getData())) {
            case POLL:
                //System.out.println("ArtPoll");
                gotArtPoll(p);
                break;
            case POLLREPLY:
                //System.out.println("PollReply");
                break;
            case OUTPUT:
                //System.out.println("Got some data!");
                gotArtData(p);
                break;
            case INVALID:
                System.err.println("Invalid oppCode");
        }
    }

    private OppCode decodeOppCode(byte[] data) {
        int oppCode = ((data[9] & 0xFF) << 8) + (data[8] & 0xFF);

        OppCode[] oppcodes = OppCode.values();
        for (OppCode o : oppcodes) {
            if (oppCode == o.value()) {
                return o;
            }
        }
        return INVALID;
    }

    private void gotArtPoll(DatagramPacket p) {
        InetAddress addr = p.getAddress();
        if (connectedControllers.isEmpty()) {
            connectedControllers.add(new ConnectedController(addr, p.getData()));
        } else {
            for (ConnectedController c : connectedControllers) {
                if (!c.addr().equals(addr)) {
                    connectedControllers.add(new ConnectedController(addr, p.getData()));
                }
            }
        }
        sendPollReply(p);
    }

    private static final PollReply pr = new PollReply();

    private void sendPollReply(DatagramPacket p) {
        //DatagramPacket packet = new DatagramPacket(pr.data(), pr.data().length, SystemInfo.broadcastAddresses.get(0), UDP_PORT_NUMBER); //IS THIS QUICKER?
        p.setData(pr.data());
        p.setPort(UDP_PORT_NUMBER);
        p.setAddress(SystemInfo.broadcastAddresses.get(0));
        udp.tms().add(p);
    }

    //gotArtData
    private ArtDMX dmx;

    private void gotArtData(DatagramPacket p) {
        if (onlySendChanges.get()) {
            onlySendChanges(p);
        } else {
            sendEveryMessage(p);

        }
    }
    
    /**
     *
     */
    public void sendData() {
        if (messagePerChannel.get()) {
            for(int i = 0; i < dmx.data().length; i++){
                hub.add(dmx.toOSCindividual(this, i));
            }
        } else {
            hub.add(dmx.toOSC(this));
        }
    }

    private void onlySendChanges(DatagramPacket p) {
        ArtDMX newDmx = new ArtDMX(p.getData());
        if (dmx == null) {
            dmx = new ArtDMX(p.getData());
        }

        if (messagePerChannel.get()) {
            byte[] newData = newDmx.data();
            byte[] oldData = dmx.data();
            int i = 0;
            while (i < newData.length && i < oldData.length) {
                if (newData[i] != oldData[i]) {
                    hub.add(newDmx.toOSCindividual(this, i));
                }
                i++;
            }
            while (i < newData.length) {
                hub.add(newDmx.toOSCindividual(this, i));
                i++;
            }
        } else if (newDmx != dmx) {
            hub.add(dmx.toOSC(this));
        }
        dmx = newDmx;
    }

    private void sendEveryMessage(DatagramPacket p) {
        dmx = new ArtDMX(p.getData());
        if (messagePerChannel.get()) {
            OSCMessage[] dmxOSC = dmx.toOSCindividual(this);
            for (OSCMessage m : dmxOSC) {
                hub.add(m);
            }
        } else {
            OSCMessage dmxOSC = dmx.toOSC(this);
            //System.out.println("message: " + dmxOSC.addressPattern() + dmxOSC.typeTags() + ": " + Arrays.toString(dmxOSC.getBlob(0)));
            hub.add(dmxOSC);
        }
    }

    //EnableMessagePerChannel
    private final AtomicBoolean messagePerChannel = new AtomicBoolean(false);

    /**
     *
     * @param b
     */
    public void enableMessagePerChannel(boolean b) {
        messagePerChannel.set(b);
    }

    //onlySendChanges
    private final AtomicBoolean onlySendChanges = new AtomicBoolean(false);

    /**
     *
     * @param b
     */
    public void onlySendChanges(boolean b) {
        onlySendChanges.set(b);
    }

    /**
     *
     */
    public void shutdown() {
        running.set(false);
    }
}
