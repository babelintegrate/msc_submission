package babel.ArtNet.Enums;

/**
 *
 * @author T R Bland
 */
public enum OppCode {

    /**
     *
     */
    INVALID(0x0),

    /**
     *
     */
    POLL(0x2000),

    /**
     *
     */
    POLLREPLY(0x2100),

    /**
     *
     */
    DIAGDATA(0x2300),

    /**
     *
     */
    COMMAND(0x2400),

    /**
     *
     */
    OUTPUT(0x5000),

    /**
     *
     */
    NZS(0x5100),

    /**
     *
     */
    SYNC(0x5200),

    /**
     *
     */
    ADDRESS(0x6000),

    /**
     *
     */
    INPUT(0x7000),

    /**
     *
     */
    TODREQUEST(0x8000),

    /**
     *
     */
    TODDATA(0x8100),

    /**
     *
     */
    TODCONTROL(0x8200),

    /**
     *
     */
    RDM(0x8300),

    /**
     *
     */
    RDMSUB(0x8400),

    /**
     *
     */
    VIDEOSETUP(0xa010),

    /**
     *
     */
    VIDEOPALETTE(0xa020),

    /**
     *
     */
    VIDEODATA(0xa040),

    /**
     *
     */
    FIRMWAREMASTER(0xF200),

    /**
     *
     */
    FIRMWAREREPLY(0xF300),

    /**
     *
     */
    FILETNMASTER(0xF400),

    /**
     *
     */
    FILEFNMASTER(0xF500),

    /**
     *
     */
    FILEFNREPLY(0xF600),

    /**
     *
     */
    IPPROG(0xF800),

    /**
     *
     */
    IPPROGREPLY(0xF900),

    /**
     *
     */
    MEDIA(0x9000),

    /**
     *
     */
    MEDIAPATCH(0x9100),

    /**
     *
     */
    MEDIACONTROL(0x9200),

    /**
     *
     */
    MEDIACONTROLREPLY(0x9300),

    /**
     *
     */
    TIMECODE(0x9700),

    /**
     *
     */
    TIMESYNC(0x9800),

    /**
     *
     */
    TRIGGER(0x9900),

    /**
     *
     */
    DIRECTORY(0x9a00),

    /**
     *
     */
    DIRECTORYREPLY(0x9b00);

    private final int value;

    private OppCode(int value) {
        this.value = value;
    }

    /**
     *
     * @return
     */
    public int value() {
        return value;
    }
    
    /**
     *
     * @return
     */
    public byte[] get(){
        byte[] toReturn = {(byte)(value & 0xFF), (byte)((value >> 8) & 0xFF)};
        return toReturn;
    }

}
