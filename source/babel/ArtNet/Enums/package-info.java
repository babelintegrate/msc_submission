/**
 * <p>Enumerated types required for parsing and creating ArtNet messages.</p>
 */
package babel.ArtNet.Enums;
