package babel.ArtNet.Enums;

/**
 *
 * @author T R Bland
 */
public enum DiagnosticPriorityCodes {

    /**
     *
     */
    NONE(0x00),

    /**
     *
     */
    DPLOW(0x10),

    /**
     *
     */
    DPMED(0x40),

    /**
     *
     */
    DPHIGH(0x80),

    /**
     *
     */
    DPCRITICAL(0xE0),

    /**
     *
     */
    DPVOLATILE(0xF),

    /**
     *
     */
    UNKNOWN(0x00);

    private final int value;
    
    private DiagnosticPriorityCodes(int value) {
        this.value = value;
    }
    
    /**
     *
     * @return
     */
    public int value(){
        return value;
    }
    
}
