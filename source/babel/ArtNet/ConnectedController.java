package babel.ArtNet;

import babel.ArtNet.Enums.DiagnosticPriorityCodes;
import static babel.ArtNet.Enums.DiagnosticPriorityCodes.UNKNOWN;
import java.net.InetAddress;

/**
 * Stores information about a connected Art-Net controller.
 * @author T R Bland
 */
public class ConnectedController {

    private final InetAddress address;

    //Info from ArtPoll
    private final int protocolVersion;
    private final boolean sendArtPollReplyOnNodeConditionChange;
    private final boolean sendDiagnostics;
    private final boolean diagnosticsAreUnicast;
    private final boolean disableVLC;
    private final DiagnosticPriorityCodes dianosticsPriority;

    /**
     *
     * @param address
     * @param data
     */
    public ConnectedController(InetAddress address, byte[] data) {
        this.address = address;
        protocolVersion = (data[10] << 8) + data[11];
        sendArtPollReplyOnNodeConditionChange = ((data[12] & 2) > 0);
        sendDiagnostics = ((data[12] & 4) > 0);
        diagnosticsAreUnicast = ((data[12] & 8) > 0);
        disableVLC = ((data[12] & 16) > 0);
        dianosticsPriority = getPriority(data[13]);
    }

    //Work out which enum the priority code is equal to and return the correct enum.
    private DiagnosticPriorityCodes getPriority(byte b) {
        DiagnosticPriorityCodes[] p = DiagnosticPriorityCodes.values();
        for(DiagnosticPriorityCodes pcode : p){
            if(b == pcode.value()){
                return pcode;
            }
        }
        return UNKNOWN;
    }
    
    InetAddress addr() {
        return address;
    }

}
