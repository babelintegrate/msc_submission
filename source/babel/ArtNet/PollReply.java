package babel.ArtNet;

import static babel.ArtNet.Enums.OppCode.POLLREPLY;
import babel.Babel;
import babel.NetTools.SystemInfo;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * For generating an art-poll replyt message
 * @author T R Bland
 */
public class PollReply {

    private static final byte[] ID = {'A', 'r', 't', '-', 'N', 'e', 't', '\0'};
    private static final byte[] OPPCODE = POLLREPLY.get();
    private static final byte[] IP = new byte[4]; //Assigned in constructor
    private static final byte[] PORT = {0x36, 0x19};
    private static final byte[] VERSINFO = Babel.VERSION;
    private static final byte NETSWITCH = 0; //WHAT IS THIS?
    private static final byte SUBSWITCH = 0; //WHAT IS THIS?
    private static final byte[] OEM = {0, 0}; //See TABLE 2
    private static final byte UBEA = 0;
    private static final byte STATUS1 = 0;   //Fill this out properly
    private static final byte[] ESTA = {'0', '0'}; //Would need to apply for this in real product
    private static final byte[] SHORTNAME = {'B', 'a', 'b', 'e', 'l', '-', 'T', 'R', 'A', 'N', 'S', 'L', 'A', 'T', 'O', 'R', '\0', '\0'};
    private static final String LONGNAMESTRING = "Babel-Translator Thomas R Bland\0";
    private static final byte[] LONGNAME = new byte[64]; //assigned in constructor
    private static final byte[] NODEREPORT = new byte[64]; //Do this properly
    private static final byte[] NUMPORTS = {0x0, 0x01}; //MAX is 4
    private static final byte[] PORTTYPES = {(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00}; //85 if can output from ANET network and output is ANET. 0x80 if output is DMX
    private static final byte[] GOODINPUT = {(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00}; //to implement
    private static final byte[] GOODOUTPUT = {(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00};//to implement
    private static final byte[] SWIN = {(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00};      //to implement
    private static final byte[] SWOUT = {(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00};     //to implement
    private static final byte SWVIDEO = 0; //DEPRICATED
    private static final byte SWMACRO = 0;
    private static final byte SWREMOTE = 0;
    private static final byte[] SPARE1 = new byte[3];
    private static final byte STYLE = 0; //A DMX to/from Art-Net device
    private static final byte[] MAC = new byte[6]; //assigned in constructor
    private static final byte[] BINDIP = new byte[4];
    private static final byte BINDINDEX = 0;
    private static final byte STATUS2 = 0; //CONFIGURE THIS PROPERLY
    private static final byte[] FILLER = new byte[26];

    private byte[] data;

    /**
     *
     */
    public PollReply() {
        try {
            InetAddress addr = SystemInfo.ipAddresses.get(0);
            NetworkInterface network = NetworkInterface.getByInetAddress(addr);
            for (int i = 0; i < MAC.length /*&& i < mac.length*/; i++) {
                //MAC[i] = mac[i];
                MAC[i] = 0;
            }
            byte[] ip = addr.getAddress();
            for (int i = 0; i < IP.length; i++) {
                IP[i] = ip[i];
            }
            char[] longname = LONGNAMESTRING.toCharArray();
            for (int i = 0; i < longname.length; i++) {
                LONGNAME[i] = (byte) longname[i];
            }
            data = build();
        } catch (SocketException ex) {
            Logger.getLogger(PollReply.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private byte[] build() {
        ArrayList<Byte> tx = new ArrayList<>();
        tx.addAll(toList(ID));
        tx.addAll(toList(OPPCODE));
        tx.addAll(toList(IP));
        tx.addAll(toList(PORT));
        tx.addAll(toList(VERSINFO));
        tx.add(NETSWITCH);
        tx.add(SUBSWITCH);
        tx.addAll(toList(OEM));
        tx.add(UBEA);
        tx.add(STATUS1);
        tx.addAll(toList(ESTA));
        tx.addAll(toList(SHORTNAME));
        tx.addAll(toList(LONGNAME));
        tx.addAll(toList(NODEREPORT));
        tx.addAll(toList(NUMPORTS));
        tx.addAll(toList(PORTTYPES));
        tx.addAll(toList(GOODINPUT));
        tx.addAll(toList(GOODOUTPUT));
        tx.addAll(toList(SWIN));
        tx.addAll(toList(SWOUT));
        tx.add(SWVIDEO);
        tx.add(SWMACRO);
        tx.add(SWREMOTE);
        tx.addAll(toList(SPARE1));
        tx.add(STYLE);
        tx.addAll(toList(MAC));
        tx.addAll(toList(BINDIP));
        tx.add(BINDINDEX);
        tx.add(STATUS2);
        tx.addAll(toList(FILLER));

        int txSize = tx.size();
        byte[] toReturn = new byte[txSize];
        for (int i = 0; i < txSize; i++) {
            toReturn[i] = tx.get(i);
        }

        return toReturn;
    }

    private ArrayList<Byte> toList(byte[] bytes) {
        ArrayList<Byte> tx = new ArrayList<>();
        for (byte b : bytes) {
            tx.add(b);
        }
        return tx;
    }

    /**
     *
     * @return
     */
    public byte[] data() {
        return data;
    }
}
