package babel.OSC;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;

/**
 *
 * @author T R Bland
 */
class Streamer {
    
    
    int[] stream(String s) {
        char[] c = s.toCharArray();
        ArrayList<Integer> builder = new ArrayList<>();

        for (char i : c) {
            builder.add((int) i);
        }
        builder.add(0);

        while (builder.size() % 4 != 0) {
            builder.add(0);
        }

        int[] toSend = new int[builder.size()];
        for (int i = 0; i < builder.size(); i++) {
            toSend[i] = builder.get(i);
        }

        return toSend;
    }

    int[] stream(int num) {
        int[] toSend = {
            (num & 0xff000000) >> 24,
            (num & 0x00ff0000) >> 16,
            (num & 0x0000ff00) >> 8,
            (num & 0x000000ff)
        };
        return toSend;
    }

    int[] stream(float num) {
        byte[] b = ByteBuffer.allocate(4).putFloat(num).array();
        int[] toSend = new int[b.length];
        for (int i = 0; i < b.length; i++) {
            toSend[i] = (int) b[i];
        }
        return toSend;
    }
    
    //DE-STREAM
    
    int toInt(int[] n) {
        int number = n[0] << 24;
        number += (n[1] & 0x000000FF) << 16;
        number += (n[2] & 0x000000FF) << 8;
        number += (n[3] & 0x000000FF);
        return number;
    }

    float toFloat(int[] n) {
        byte[] b = new byte[4];
        for (int i = 0; i < n.length; i++) {
            b[i] = (byte) (n[i] & 0x000000FF);
        }
        return ByteBuffer.wrap(b).order(ByteOrder.BIG_ENDIAN).getFloat();
    }
}
