package babel.OSC;

import babel.NetTools.UDPserver;
import java.net.DatagramPacket;
import java.net.InetSocketAddress;
import java.net.SocketException;

/**
 *
 * @author T R Bland
 */
public class OSCudpServer extends OSCserver {

    private final UDPserver server;
    private final Parser parser = new Parser(this);
    private final InetSocketAddress destination;

    /**
     *
     * @param messagesToHub Send messages directly to the Hub
     * @param allowFeedback
     * @param destination Destination host and port
     * @throws java.net.SocketException
     */
    public OSCudpServer(boolean messagesToHub, boolean allowFeedback, InetSocketAddress destination) throws SocketException {
        super("OSCudpServer", messagesToHub, allowFeedback);
        server = new UDPserver(destination.getPort());
        this.destination = destination;
    }

    @Override
    void startUpJobs() {
        server.start();
    }

    @Override
    void runningJobs() {
        if (server.messages.check()) {
            try {
                DatagramPacket m = (DatagramPacket) server.messages.get();
                OSCMessage message = parser.parse(m.getData());
                if (message != null) {
                    //System.out.println("\033[36m" + new java.util.Date() + " - OSC UDP RX: " + message.addressPattern() + message.typeTags() + " " + message.getDataAsString() + "\033[0m");
                    super.add(message);
                }
            } catch (Exception ex) {
                System.err.println("OSCudpServer.runningJobs: new message: COULD NOT GET MESSAGE " + ex.toString());
            }
        }
    }

    /**
     * For broadcast messages
     *
     * @param m
     */
    @Override
    public void send(OSCMessage m) {
        if (super.allowfeedback()) {
            //System.out.println("\033[36m" + new java.util.Date() + " - OSC UDP TX: " + m.addressPattern() + m.typeTags() + " " + m.getDataAsString() + "\033[0m");
            server.sendMessage(m.stream(), destination);
        } else if (m.source() != this) {
            //System.out.println("\033[36m" + new java.util.Date() + " - OSC UDP TX: " + m.addressPattern() + m.typeTags() + " " + m.getDataAsString() + "\033[0m");
            server.sendMessage(m.stream(), destination);
        }
    }

    @Override
    void closing() {
        server.shutdown();
        parser.shutdown();
    }

    @Override
    void decode(OSCMessage m) {
        send(m);
    }

}
