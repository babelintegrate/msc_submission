package babel.OSC;

import java.util.LinkedList;

/**
 *
 * @author T R Bland
 */
class Extractor {
    private static final Streamer streamer = new Streamer();
    
    int i(LinkedList<Integer> buffer) {
        return streamer.toInt(new int[]{buffer.poll(), buffer.poll(), buffer.poll(), buffer.poll()});
    }

    float f(LinkedList<Integer> buffer) {
        return streamer.toFloat(new int[]{buffer.poll(), buffer.poll(), buffer.poll(), buffer.poll()});
    }

    String s(LinkedList<Integer> buffer) {
        String s = "";
        int counter = 0;
        while (buffer.peek() != 0) {
            int intChar = buffer.poll();
            char c = (char) intChar;
            s = s + c;
            counter++;
        }
        
        //Null terminated string so read in null
        int temp = buffer.poll();
        counter++;
        if(temp != 0){
            System.err.println("babel.OSC.OSCMessage.extractData: just read a null that wasn't null");
        }
        
        while (counter % 4 != 0) {
            if (buffer.poll() != 0) {
                System.err.println("babel.OSC.OSCMessage.extractData: just read a null that wasn't null");
            }
            counter++;
        }
        return s;
    }

    int[] b(LinkedList<Integer> buffer) {
        int length = buffer.poll();
        int[] toReturn = new int[length+1];
        toReturn[0] = length;
        for (int j = 1; j < length; j++) {
            toReturn[j] = buffer.poll();
        }
        //System.out.println("Blob: " + Arrays.toString(toReturn));
        return toReturn;
    }
}
