/**
 * Classes used for implemening the OSC protocol.
 * Because this is the protocol used for implementing internal communications it is not a standard "module". It also provides OSC Servers and Clients (TCP and UDP).
 */
package babel.OSC;
