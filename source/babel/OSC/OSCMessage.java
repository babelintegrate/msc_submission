package babel.OSC;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

/**
 * For checking messages: https://www.branah.com/ascii-converter
 *
 * @author T R Bland
 */
public class OSCMessage {
//49, 57, 50, 46, 49, 54, 56, 46, 50, 46, 50, 53, 0, 0, 0, 0, 116, 101, 115, 116, 50, 0, 0, 0

    private static final Streamer STREAMER = new Streamer();
    private static final Extractor EXTRACTOR = new Extractor();
    private Object source = null, translationSource = null;
    private String addressPattern, textMessage;
    private final String typeTags;
    private int rawData[];

    private final ArrayList<Object> data;

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        int[] d = {49, 57, 50, 46, 49, 54, 56, 46, 50, 46, 50, 53, 0, 0, 0, 0, 116, 101, 115, 116, 50, 0, 0, 0};
        OSCMessage m = new OSCMessage(null, "/system/osc/server/add/udp", ",ss", d);
        if (!(m.addressPattern().equals("/system/osc/server/add/udp") && m.typeTags().equals(",ss") && m.getDataAsString().equals("192.168.2.25test2"))) {
            System.err.println("FAILED: Parsed as: " + m.addressPattern() + m.typeTags() + " " + m.getDataAsString());
            System.err.println("stream of OSC: " + Arrays.toString(m.stream()));
            System.err.println("String0: " + m.getString(0) + " string1: " + m.getString(1));
        } else {
            System.out.println("Passed: Parsed as: " + m.addressPattern() + m.typeTags() + " " + m.getDataAsString());
        }
    }

    //--------------------------------------------------------------------------
    OSCMessage(Object source, String ap, String tt, int[] d) {
        this.addressPattern = ap;
        testAP();
        this.typeTags = tt;
        this.rawData = d;
        this.data = extractData(d);
        this.source = source;
        textMessage = buildTextMessage();
    }

    /**
     *
     * @param source
     * @param ap
     * @param d
     */
    public OSCMessage(Object source, String ap, String d) {
        this.addressPattern = ap;
        testAP();
        this.typeTags = ",s";
        this.rawData = buildRawData(STREAMER.stream(d));
        this.data = new ArrayList<>();
        this.data.add(d);
        this.source = source;
        textMessage = buildTextMessage();
    }

    /**
     *
     * @param source
     * @param ap
     * @param d
     */
    public OSCMessage(Object source, String ap, int d) {
        this.addressPattern = ap;
        testAP();
        this.typeTags = ",i";
        this.rawData = buildRawData(STREAMER.stream(d));
        this.data = new ArrayList<>();
        this.data.add(d);
        this.source = source;
        textMessage = buildTextMessage();
    }

    /**
     *
     * @param source
     * @param ap
     * @param d
     */
    public OSCMessage(Object source, String ap, float d) {
        this.addressPattern = ap;
        testAP();
        this.typeTags = ",f";
        this.rawData = buildRawData(STREAMER.stream(d));
        this.data = new ArrayList<>();
        this.data.add(d);
        this.source = source;
        textMessage = buildTextMessage();
    }

    /**
     *
     * @param source
     * @param ap
     * @param d
     */
    public OSCMessage(Object source, String ap, Type d) {
        this.addressPattern = ap;
        testAP();
        this.typeTags = "," + d.get();
        this.data = null;
        this.source = source;
        textMessage = buildTextMessage();
    }

    //--------------------------------------------------------------------------
    private void testAP() {
        if (!addressPattern.startsWith("/")) {
            String ap = addressPattern;
            addressPattern = "/" + ap;
        }
        /*if(!addressPattern.endsWith("\0")){
            addressPattern += "\0";
        }
        while(addressPattern.length() % 4 != 0){
            addressPattern += "\0";
        }*/
    }

    private int[] buildRawData(int[] data) {
        ArrayList<Integer> builder = new ArrayList<>();

        for (int i : data) {
            builder.add(i);
        }

        int toReturn[] = new int[builder.size()];
        for (int i = 0; i < builder.size(); i++) {
            toReturn[i] = builder.get(i);
        }
        return toReturn;
    }

    private ArrayList<Object> extractData(int[] rawData) {
        ArrayList<Object> d = new ArrayList<>();
        LinkedList<Integer> buffer = new LinkedList<>();
        for (int i : rawData) {
            buffer.add(i);
        }

        char[] tt = typeTags.toCharArray();
        for (int i = 0; i < tt.length; i++) {
            switch (tt[i]) {
                case 'i':
                    d.add(EXTRACTOR.i(buffer));
                    break;
                case 'f':
                    d.add(EXTRACTOR.f(buffer));
                    break;
                case 's':
                    d.add(EXTRACTOR.s(buffer));
                    break;
                case 'b':
                    d.add(EXTRACTOR.b(buffer));
                    break;
                default:
            }
        }
        return d;
    }

    //GETTERS-------------------------------------------------------------------
    /**
     *
     * @return
     */
    public String addressPattern() {
        return addressPattern;
    }

    /**
     *
     * @return
     */
    public String typeTags() {
        return typeTags;
    }

    /**
     *
     * @return
     */
    public String typeTagsNoComma() {
        return typeTags.substring(1);
    }

    /**
     *
     * @return
     */
    public String getDataAsString() {
        String s = "";
        for (int i = 0; i < data.size(); i++) {
            s += data.get(i).toString();
        }
        return s;
    }

    /**
     *
     * @param i
     * @return
     */
    public String getString(int i) {
        try {
            String s = (String) data.get(i);
            return s;
        } catch (Exception ex) {
            System.err.println("OSCMessage.getString: " + ex.getLocalizedMessage());
            return "Internal Error";
        }
    }

    /**
     *
     * @param i
     * @return
     */
    public int getInt(int i) {
        return (int) data.get(i);
    }

    /**
     *
     * @param i
     * @return
     */
    public int[] getBlob(int i) {
        return (int[]) data.get(i);
    }

    /**
     *
     * @return
     */
    public Object source() {
        return source;
    }

    /**
     *
     * @return
     */
    public int[] stream() {
        int[] ap = STREAMER.stream(addressPattern);
        ArrayList<Integer> builder = new ArrayList<>();
        for (int i : ap) {
            builder.add(i);
        }

        int[] tt = STREAMER.stream(typeTags);
        for (int i : tt) {
            builder.add(i);
        }

        for (int i : rawData) {
            builder.add(i);
        }

        int[] toSend = new int[builder.size()];
        for (int i = 0; i < builder.size(); i++) {
            toSend[i] = builder.get(i);
        }
        return toSend;
    }
    
    private String buildTextMessage(){
        return addressPattern + typeTags + "," + textMessageArgs();
    }

    /**
     *
     * @return
     */
    public String textMessage() {
        return textMessage;
    }

    private String textMessageArgs() {
        String toReturn = "";
        char[] tts = typeTags.toCharArray();
        int j = 0;
        for (int i = 1; i < tts.length; i++) {
            if(j != 0){
                toReturn += "|";
            }
            switch (tts[i]) {
                case 's':
                    toReturn += data.get(j);
                    j++;
                    break;
                case 'i':
                    int tempInt = (int) data.get(j);
                    toReturn += tempInt;
                    j++;
                    break;
                case 'f':
                    float tempFloat = (float) data.get(j);
                    toReturn += tempFloat;
                    j++;
                    break;
                case 'T':
                case 'F':
                case 'N':
                    break;
                case 'b':
                    break;
            }
        }
        return toReturn;
    }
    
    /**
     *
     * @return
     */
    public Object translationSource(){
        return translationSource;
    }

    //*******************************************************
    /**
     *
     * @param source
     */
    public void source(Object source) {
        this.source = source;
    }
    
    /**
     *
     * @param source
     */
    public void translationSource(Object source){
        this.translationSource = source;
    }

}
