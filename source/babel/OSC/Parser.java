package babel.OSC;

import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * To be called with new data. If the new data does not contain a full message
 * the buffer is maintained an null is returned from parse(). However, if there
 * is a full message then the Message is returned. The buffer is mainteained by
 * a timer, that clears the buffer ever 300ms unless it is empty or starts with
 * a '/'. This stops corrupted messages freezing the sustem.
 *
 * IF YOU UPDATE THIS YOU MUST UPDATE PARSER.JS
 *
 * @author T R Bland
 */
public class Parser {

    private final Object source;
    private final BuildingState bs = new BuildingState();
    private final LinkedList<Integer> buffer;

    private int bufSizeWatcher = 0;
    private final Timer cleanBuffer;
    private final TimerTask timeToClean = new TimerTask() {
        @Override
        public void run() {
            int size = buffer.size();
            if (size > 0 && size == bufSizeWatcher) {
                while (buffer.size() > 0 && buffer.get(0) != 0x2F) {
                    buffer.poll();
                }
                bs.reset();
                size = buffer.size();
                parse();
            }
            bufSizeWatcher = size;
        }
    };

    private String ap;
    private String tt;
    private int[] d;

    void shutdown() {
        //timeToClean.cancel();
        cleanBuffer.cancel();
    }

    final class BuildingState {

        boolean gotAP = false;
        boolean gotTT = false;
        boolean gotData = false;

        void reset() {
            gotAP = false;
            gotTT = false;
            gotData = false;
        }
    }

    /**
     *
     * @param source
     */
    public Parser(Object source) {
        this.source = source;
        buffer = new LinkedList<>();
        cleanBuffer = new Timer("OSC Parser Timer " + source.toString());
        cleanBuffer.scheduleAtFixedRate(timeToClean, 300, 300); //run every 300ms
    }

    /**
     *
     * @param data
     * @return
     */
    public OSCMessage parse(byte[] data) {
        for (int i : data) {
            buffer.offer(i);
        }
        return parse();
    }

    /**
     *
     * @param data
     * @return
     */
    public OSCMessage parse(int[] data) {
        for (int i : data) {
            buffer.offer(i);
        }
        return parse();
    }

    private OSCMessage parse() {
        if (readyToGetAP()) {
            ap = getAP();
            bs.gotAP = true;
        }

        if (readyToGetTT()) {
            tt = getTT();
            bs.gotTT = true;
        }

        if (readyToGetData()) {
            d = getData();
            if (d != null) {
                bs.gotData = true;
            }
        }

        if (gotFullMessage()) {
            //System.out.println(ap + tt + " " + Arrays.toString(d));
            bs.reset();
            return new OSCMessage(source, ap, tt, d);
        }
        return null;//if there is no full message yet
    }

    private boolean readyToGetAP() {
        boolean b = !bs.gotAP && !bs.gotTT && !bs.gotData && indexOfComma() > 0;
        return b;
    }

    private boolean readyToGetTT() {
        boolean b = bs.gotAP && !bs.gotTT && !bs.gotData && indexOfComma() == 0 && indexOfNull() > 0 && enoughForTT();
        return b;
    }

    private boolean readyToGetData() {
        boolean b = bs.gotAP && bs.gotTT && !bs.gotData && enoughData();
        return b;
    }

    private boolean gotFullMessage() {
        boolean b = bs.gotAP && bs.gotTT && bs.gotData;
        return b;
    }

    private String getAP() {
        String builder = "";
        while (buffer.peek() != 0x2C) {
            int i = buffer.poll();
            char c = (char) i;
            if (c != '\0') {
                builder = builder + c;
            }
        }
        return builder;
    }

    private String getTT() {
        String builder = "";
        int bytesRead = 0;

        while (buffer.peek() != 0) {
            int i = buffer.poll();
            char c = (char) i;
            builder = builder + c;
            bytesRead++;
        }
        while (bytesRead % 4 != 0) {
            if (buffer.poll() != 0) {
                System.err.println("OSC.Parser.getTT just threw away real data.");
            }
            bytesRead++;
        }
        return builder;
    }

    private int[] getData() {
        int count = calculateHowMuchData();
        if (count > buffer.size()) {
            System.err.println("babel.OSC.Parser.getData Dont have enough data");
        } else {
            int[] a = new int[count];
            for (int j = 0; j < count; j++) {
                a[j] = buffer.poll();
            }
            return a;
        }
        return null;
    }
    
    //THIS BUFFER WILL OVERFLOW IF THERE ISN'T ENOUGH DATA!
    private int calculateHowMuchData() {
        int count = 0;
        //for each TT
        for (int i = 0; i < tt.length(); i++) {
            switch (tt.toCharArray()[i]) {
                case 'f':
                case 'i':
                    count += 4;
                    break;
                case 's':
                    for (int j = count; buffer.get(j) != 0 && j < buffer.size(); j++) {
                        count += 1;
                    }
                    count++; //to account for the \0
                    while (count % 4 != 0) {
                        count++;
                    }
                    break;
                case 'b':
                    int[] n = new int[4];
                    for (int j = 0; j < 4; j++) {
                        n[j] = buffer.get(j + count);
                    }
                    int numBytes = (toInt(n) + 1) * 4;
                    count += numBytes;
                    break;
                default:
            }
        }
        return count;
    }

    private boolean enoughForTT() {
        int n = indexOfNull();
        int r = (4 - (n % 4));
        return n > 0 && buffer.size() >= (n + r);
    }

    private boolean enoughData() {
        int index = 0;
        int s = buffer.size();
        int numberOfData = tt.length();

        for (int i = 0; i < numberOfData; i++) {
            switch (tt.toCharArray()[i]) {
                case ',':
                    break;
                case 'i':
                    if (s < (index + 4)) {
                        return false;
                    }
                    index += 4;
                    break;
                case 'f':
                    if (s < (index + 4)) {
                        return false;
                    }
                    index += 4;
                    break;
                case 's':
                    int count = 0;
                    boolean foundNull = false;
                    while (!foundNull && index < s) {
                        if (buffer.get(index) == 0) {
                            foundNull = true;
                        } else {
                            count++;
                            index++;
                        }
                    }
                    //check packed with enough nulls
                    while (count % 4 != 0 && index < s) {
                        if (buffer.get(index) != 0) {
                            return false;
                        }
                        count++;
                        index++;
                    }
                    if (index > s) {
                        return false;
                    }
                    break;
                case 'b':
                    count = index;
                    int[] n = new int[4];
                    for (int j = 0; j < 4; j++) {
                        n[j] = buffer.get(j + count);
                    }
                    int numBytes = (toInt(n) + 1) * 4;
                    index += (numBytes + 1) * 4;
                    break;
                default:
            }
        }
        return true;
    }

    private int indexOfComma() {
        return buffer.indexOf(0x2C);
    }

    private int indexOfNull() {
        return buffer.indexOf(0);
    }

    private int toInt(int[] n) {
        int number = n[0] << 24;
        number += (n[1] & 0x000000FF) << 16;
        number += (n[2] & 0x000000FF) << 8;
        number += (n[3] & 0x000000FF);
        return number;
    }

}
