package babel.OSC;

import babel.Hub.Hub;
import babel.TMS.TMS;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * OSCserver can send direct to hub, or can be polled. OSCserver can also block
 feedback or allow it.
 *
 * @author T R Bland
 */
abstract class OSCserver extends Thread {

    private final AtomicBoolean running = new AtomicBoolean(false);
    private final Hub hub = Hub.get();
    private final TMS messagesFromHub;
    public TMS messages = new TMS();
    private final boolean messagesToHub;
    private final boolean allowFeedback;

    public OSCserver(String name, boolean messagesToHub, boolean allowFeedback) {
        super(name);
        this.messagesToHub = messagesToHub;
        this.allowFeedback = allowFeedback;

        if (messagesToHub) {
            messagesFromHub = hub.register();
        } else {
            messagesFromHub = null;
        }
    }

    @Override
    public void run() {
        startUpJobs();
        running.set(true);
        while (running.get()) {
            if (messagesToHub) {
                checkHub();
            }
            runningJobs();
            sleep();
        }
    }

    private void checkHub() {
        if (messagesFromHub.check()) {
            try {
                OSCMessage m = messagesFromHub.get();
                if (!m.addressPattern().startsWith("/system/")) {
                    decode(m);
                }
            } catch (Exception ex) {
                System.out.println("OSC Server.java CheckHub: " + ex.getLocalizedMessage());
            }
        }
    }

    abstract void startUpJobs();

    abstract void runningJobs();

    abstract public void send(OSCMessage m);

    abstract void closing();

    abstract void decode(OSCMessage m);

    private void sleep() {
        try {
            Thread.sleep(0,1);
        } catch (InterruptedException e) {
            System.out.println("Exception in Pinger");
        }
    }

    void add(OSCMessage m) {
        if (messagesToHub) {
            hub.add(m);
        } else {
            messages.add(m);
        }
    }

    //Setters-------------------------------------------------------------------
    public void shutdown() {
        closing();
        running.set(false);
    }

    //Getters-------------------------------------------------------------------
    public boolean allowfeedback() {
        return allowFeedback;
    }
}
