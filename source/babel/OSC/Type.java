package babel.OSC;

/**
 *
 * @author T R Bland
 */
public enum Type {

    /**
     *
     */
    True("T"),

    /**
     *
     */
    False("F"),

    /**
     *
     */
    Impulse("I"),

    /**
     *
     */
    Null("N");
    
    private final String val;

    Type(String val) {
        this.val = val;
    }

    /**
     *
     * @return
     */
    public String get() {
        return val;
    }
}
