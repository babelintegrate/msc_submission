package babel.OSC;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author T R Bland
 */
public class OSCMessageBuilder {

    private final Object source;
    private final String addressPattern;
    private final ArrayList<Character> typeTag = new ArrayList<>();
    private final ArrayList<Integer> rawData = new ArrayList<>();

    private final Streamer streamer = new Streamer();

    /**
     *
     * @param source
     * @param addressPattern
     */
    public OSCMessageBuilder(Object source, String addressPattern) {
        this.source = source;
        this.addressPattern = addressPattern;
        typeTag.add(',');
    }
    
    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        
        OSCMessageBuilder mb = new OSCMessageBuilder(null, "/system/osc/server/add/udp");
        mb.add("192.168.2.25");
        mb.add("test2");
        OSCMessage m = mb.finalise();
        if (!(m.addressPattern().equals("/system/osc/server/add/udp") && m.typeTags().equals(",ss") && m.getDataAsString().equals("192.168.2.25test2"))) {
            System.err.println("FAILED: Parsed as: " + m.addressPattern() + m.typeTags() + " " + m.getDataAsString());
            System.err.println("stream of OSC: " + Arrays.toString(m.stream()));
        } else {
            System.out.println("Passed: Parsed as: " + m.addressPattern() + m.typeTags() + " " + m.getDataAsString());
        }
    }

    /**
     *
     * @param s
     */
    public void add(String s) {
        typeTag.add('s');
        String temp = s;
        temp += "\0";
        while(temp.length() % 4 != 0){
            temp += "\0";
        }
        int[] tempArray = streamer.stream(s);
        for (int i : tempArray) {
            rawData.add(i);
        }
    }

    /**
     *
     * @param i
     */
    public void add(int i) {
        typeTag.add('i');
        int[] temp = streamer.stream(i);
        for (int j : temp) {
            rawData.add(j);
        }
    }

    /**
     *
     * @param f
     */
    public void add(float f) {
        typeTag.add('f');
        int[] temp = streamer.stream(f);
        for (int i : temp) {
            rawData.add(i);
        }
    }
    
    /**
     *
     * @param i
     */
    public void add(int[] i){
        typeTag.add('b');
        rawData.add(i.length);
        for (int j : i) {
            rawData.add(j);
        }
    }
    
    /**
     *
     * @param b
     */
    public void add(byte[] b){
        typeTag.add('b');
        int[] len = streamer.stream(b.length);
        for(int l : len){
            rawData.add(l);
        }
        for (byte j : b) {
            rawData.add(j & 0xFF);
        }
    }

    /**
     *
     * @param b
     */
    public void add(Boolean b) {
        if (b) {
            typeTag.add('T');
        } else {
            typeTag.add('F');
        }
    }

    /**
     *
     */
    public void addImpulse() {
        typeTag.add('I');
    }

    /**
     *
     */
    public void addNull() {
        typeTag.add('N');
    }

    /**
     *
     * @return
     */
    public OSCMessage finalise() {
        String tt = "";
        for(Character c : typeTag){
            tt += c;
        }
        int[] d = new int[rawData.size()];
        
        for(int i = 0; i < d.length; i++){
            d[i] = rawData.remove(0);
        }
        return new OSCMessage(source, addressPattern, tt, d);
    }

}
