package babel.OSC;

import babel.NetTools.TCPMessage;
import babel.NetTools.TCPserver;

/**
 *
 * @author T R Bland
 */
public class OSCtcpServer extends OSCserver {

    private final TCPserver server;
    private final Parser parser = new Parser(this);

    /**
     *
     * @param messagesToHub
     * @param allowFeedback
     * @param port
     */
    public OSCtcpServer(boolean messagesToHub, boolean allowFeedback, int port) {
        super("OSCtcpServer", messagesToHub, allowFeedback);
        server = new TCPserver(port);
    }

    @Override
    void startUpJobs() {
        server.start();
    }

    @Override
    void runningJobs() {
        if (server.messages.check()) {
            try {
                TCPMessage m = (TCPMessage) server.messages.get();
                OSCMessage message = parser.parse(m.data());
                if (message != null) {
                    //System.out.println("\033[36m" + new java.util.Date() + " - OSC TCP RX: " + message.addressPattern() + message.typeTags() + " " + message.getDataAsString() + "\033[0m");
                    super.add(message);
                }
            } catch (Exception ex) {
                System.err.println("OSC.TCPServer.runningJobs: new message: COULD NOT GET MESSAGE " + ex.toString());
            }
        }
    }

    /**
     *
     * @param m
     */
    @Override
    public void send(OSCMessage m) {
        if (super.allowfeedback()) {
            //System.out.println("\033[36m" + new java.util.Date() + " - OSC TCP TX: " + m.addressPattern() + m.typeTags() + " " + m.getDataAsString() + "\033[0m");
            server.sendMessage(m.stream());
        } else if (m.source() != this) {
            //System.out.println("\033[36m" + new java.util.Date() + " - OSC TCP TX: " + m.addressPattern() + m.typeTags() + " " + m.getDataAsString() + "\033[0m");
            server.sendMessage(m.stream());
        }
    }

    @Override
    void closing() {
        server.shutdown();
        parser.shutdown();
    }
    
    @Override
    void decode(OSCMessage m) {
        send(m);
        System.out.println("Message in OSC TCPserver");
    }

}
