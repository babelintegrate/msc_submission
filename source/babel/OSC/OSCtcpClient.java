package babel.OSC;

import babel.Hub.Hub;
import babel.NetTools.TCPMessage;
import babel.NetTools.TCPconnection;
import babel.TMS.TMS;
import babel.TMS.TypelessTMS;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 *
 * @author T R Bland
 */
public class OSCtcpClient extends Thread {

    private final Hub hub = Hub.get();
    private final AtomicBoolean running = new AtomicBoolean(false);
    private TMS messagesFromHub;
    private final TypelessTMS rx;
    private final TCPconnection connection;
    private final Parser parser = new Parser(this);
    private final boolean messagesToHub;
    private final boolean allowFeedback;

    /**
     *
     * @param messagesToHub
     * @param allowFeedback
     * @param address
     * @param port
     * @throws UnknownHostException
     * @throws Exception
     */
    public OSCtcpClient(boolean messagesToHub, boolean allowFeedback, String address, int port) throws UnknownHostException, Exception {
        super("OSCtcpClient");
        this.messagesToHub = messagesToHub;
        this.allowFeedback = allowFeedback;
        rx = new TypelessTMS();
        String[] stringAddr = address.split("\\.");
        if (stringAddr.length > 4 || stringAddr.length == 0) {
            throw new Exception("OSCtcpClient Invalid Address: " + Arrays.toString(stringAddr));
        }
        byte[] ip = new byte[4];
        for (int i = 0; i < stringAddr.length; i++) {
            ip[i] = (byte) Integer.parseInt(stringAddr[i]);
        }

        connection = new TCPconnection(InetAddress.getByAddress(ip), port, rx);
    }

    @Override
    public void run() {
        running.set(true);
        messagesFromHub = hub.register();
        while (running.get()) {
            sleep();
            checkConnection();
            checkHub();
        }
        connection.shutdown();
    }

    private void sleep() {
        try {
            Thread.sleep(0, 1);
        } catch (InterruptedException e) {
            System.out.println("Exception in Pinger" + e.getLocalizedMessage());
        }
    }

    private void checkConnection() {
        if (rx.check()) {
            try {
                TCPMessage m = (TCPMessage) rx.get();
                OSCMessage message = parser.parse(m.data());
                if (message != null) {
                    //System.out.println("\033[36m" + new java.util.Date() + " - OSC UDP RX: " + message.addressPattern() + message.typeTags() + " " + message.getDataAsString() + "\033[0m");
                    if (messagesToHub) {
                        hub.add(message);
                    } else {
                        System.out.println("\033[36m" + new java.util.Date() + " - OSC UDP RX: " + message.addressPattern() + message.typeTags() + " " + message.getDataAsString() + "\033[0m");
                    }
                }
            } catch (Exception ex) {
                System.err.println("OSC.udpServer.checkConnection: COULD NOT GET MESSAGE " + ex.toString());
            }
        }
    }

    private void checkHub() {
        if (messagesFromHub.check()) {
            try {
                OSCMessage m = messagesFromHub.get();
                if (allowFeedback) {
                    connection.send(m.stream());
                } else {
                    if(m.source() != this){
                        connection.send(m.stream());
                    }
                }
            } catch (Exception ex) {
                System.err.println("OSCtcpClient.checkHub: " + ex.getLocalizedMessage());
            }
        }
    }

    /**
     *
     */
    public void shutdown() {
        running.set(false);
        parser.shutdown();
    }
    
    
}
