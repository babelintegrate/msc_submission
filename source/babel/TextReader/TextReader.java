package babel.TextReader;

import babel.OSC.OSCMessage;
import babel.OSC.OSCMessageBuilder;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * <p>
 * For reading OSC messags in a propriatory script language.</p>
 * <table summary="Rules">
 * <tr>
 * <td>#</td>
 * <td>comment</td>
 * </tr>
 * <tr>
 * <td>,</td>
 * <td>dividor between parts of message</td>
 * </tr>
 * <tr>
 * <td>|</td>
 * <td>dividor between arguments</td>
 * </tr>
 * <tr>
 * <td>1.2.3.4.252.253.254.255</td>
 * <td>A blob</td>
 * </tr>
 * </table>
 * <p>
 * Example: /system/osc/add/server/udp,Fsi,192.168.1.3|10023</p>
 * <p></p>
 * <p>
 * DO NOT USE: ,| anywhere other than for splitting up the message</p>
 *
 * @author T R Bland
 */
public class TextReader {

    private final static TextReader tr = new TextReader();

    /**
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {

        OSCMessage m = tr.read("/smallbyte,i,12");
        if (!(m.addressPattern().equals("/smallbyte") && m.typeTags().equals(",i") && m.getDataAsString().equals("12"))) {
            System.err.println("FAILED: Parsed as: " + m.addressPattern() + m.typeTags() + " " + m.getDataAsString());
        } else {
            System.out.println("Passed: Parsed as: " + m.addressPattern() + m.typeTags() + " " + m.getDataAsString());
        }

        m = tr.read("/middlebyte,i,127");
        if (!(m.addressPattern().equals("/middlebyte") && m.typeTags().equals(",i") && m.getDataAsString().equals("127"))) {
            System.err.println("FAILED: Parsed as: " + m.addressPattern() + m.typeTags() + " " + m.getDataAsString());
        } else {
            System.out.println("Passed: Parsed as: " + m.addressPattern() + m.typeTags() + " " + m.getDataAsString());
        }

        m = tr.read("/bigbyte,i,255");
        if (!(m.addressPattern().equals("/bigbyte") && m.typeTags().equals(",i") && m.getDataAsString().equals("255"))) {
            System.err.println("FAILED: Parsed as: " + m.addressPattern() + m.typeTags() + " " + m.getDataAsString());
        } else {
            System.out.println("Passed: Parsed as: " + m.addressPattern() + m.typeTags() + " " + m.getDataAsString());
        }

        m = tr.read("/system/osc/server/add/udp,Fssi,192.168.1.3|test|10023");
        if (!(m.addressPattern().equals("/system/osc/server/add/udp") && m.typeTags().equals(",Fssi") && m.getDataAsString().equals("192.168.1.3test10023"))) {
            System.err.println("FAILED: Parsed as: " + m.addressPattern() + m.typeTags() + " " + m.getDataAsString());
        } else {
            System.out.println("Passed: Parsed as: " + m.addressPattern() + m.typeTags() + " " + m.getDataAsString());
        }

        m = tr.read("/system/rule,ssT,[/midi/0/176,ii,<1:i:F:0-8>|<2:i:LIN:0-127>]|[/ch/<1:i:F:0-8>/mix/fader,f,<2:f:LIN:0-1>]");
        if (!(m.addressPattern().equals("/system/rule") && m.typeTags().equals(",ssT") && m.getDataAsString().equals("[/midi/0/176,ii,<1:i:F:0-8>|<2:i:LIN:0-127>][/ch/<1:i:F:0-8>/mix/fader,f,<2:f:LIN:0-1>]"))) {
            System.err.println("FAILED: Parsed as: " + m.addressPattern() + m.typeTags() + " " + m.getDataAsString());
        } else {
            System.out.println("Passed: Parsed as: " + m.addressPattern() + m.typeTags() + " " + m.getDataAsString());
        }

        m = tr.read("/system/rule,ss,[/dmx/0/<1:i:F:-1:0-31>,i,<2:i:LIN:0:0-255>]|[/ch/<1:i:F:+1:1-32>/mix/fader,f,<2:f:LIN:0:0-1>]");
        if (!(m.addressPattern().equals("/system/rule") && m.typeTags().equals(",ss") && m.getDataAsString().equals("[/dmx/0/<1:i:F:-1:0-31>,i,<2:i:LIN:0:0-255>][/ch/<1:i:F:+1:1-32>/mix/fader,f,<2:f:LIN:0:0-1>]"))) {
            System.err.println("FAILED: Parsed as: " + m.addressPattern() + m.typeTags() + " " + m.getDataAsString());
        } else {
            System.out.println("Passed: Parsed as: " + m.addressPattern() + m.typeTags() + " " + m.getDataAsString());
        }

        m = tr.read("/yes,ssss,test1|test2|test3|test4");
        if (!(m.addressPattern().equals("/yes") && m.typeTags().equals(",ssss") && m.getDataAsString().equals("test1test2test3test4"))) {
            System.err.println("FAILED: Parsed as: " + m.addressPattern() + m.typeTags() + " " + m.getDataAsString());
        } else {
            System.out.println("Passed: Parsed as: " + m.addressPattern() + m.typeTags() + " " + m.getDataAsString());
        }

        m = tr.read("/test,siTFf,test1|400|25.55");
        if (!(m.addressPattern().equals("/test") && m.typeTags().equals(",siTFf") && m.getDataAsString().equals("test140025.55"))) {
            System.err.println("FAILED: Parsed as: " + m.addressPattern() + m.typeTags() + " " + m.getDataAsString());
        } else {
            System.out.println("Passed: Parsed as: " + m.addressPattern() + m.typeTags() + " " + m.getDataAsString());
        }

        m = tr.read("/false,F");
        if (!(m.addressPattern().equals("/false") && m.typeTags().equals(",F") && m.getDataAsString().isEmpty())) {
            System.err.println("FAILED: Parsed as: " + m.addressPattern() + m.typeTags() + " " + m.getDataAsString());
        } else {
            System.out.println("Passed: Parsed as: " + m.addressPattern() + m.typeTags() + " " + m.getDataAsString());
        }

        m = tr.read("/system/osc/server/add/udp,ss,192.168.2.25|test2");
        if (!(m.addressPattern().equals("/system/osc/server/add/udp") && m.typeTags().equals(",ss") && m.getDataAsString().equals("192.168.2.25test2"))) {
            System.err.println("FAILED: Parsed as: " + m.addressPattern() + m.typeTags() + " " + m.getDataAsString());
        } else {
            System.out.println("Passed: Parsed as: " + m.addressPattern() + m.typeTags() + " " + m.getDataAsString());
        }

        m = tr.read("/messagetimer,si,[/xremote]|9");
        if (!(m.addressPattern().equals("/messagetimer") && m.typeTags().equals(",si") && m.getDataAsString().equals("[/xremote]9"))) {
            System.err.println("FAILED: Parsed as: " + m.addressPattern() + m.typeTags() + " " + m.getDataAsString());
        } else {
            System.out.println("Passed: Parsed as: " + m.addressPattern() + m.typeTags() + " " + m.getDataAsString());
        }
    }

    /**
     *
     * @return
     */
    public static TextReader get() {
        return tr;
    }

    /**
     *
     * @param s
     * @return
     * @throws Exception
     */
    public OSCMessage read(String s) throws Exception {
        if (s.startsWith("/system/rule")) {
            return rule(s);
        } else if (s.startsWith("/system/messagetimer")) {
            return messageTimerMessage(s);
        }
        return normalMessage(s);
    }

    private OSCMessage rule(String s) throws Exception {
        String[] splitMessage = s.split(", *(?=[^\\]]*?(?:\\[|$))");

        String ap = splitMessage[0];
        String tt = splitMessage[1];
        String[] argsArray = splitMessage[2].split("\\| *(?=[^\\]]*?(?:\\[|$))");

        return makeMessage(tt, ap, argsArray);
    }

    private OSCMessage messageTimerMessage(String s) throws Exception {
        String[] splitMessage = s.split(", *(?=[^\\]]*?(?:\\[|$))");

        String ap = splitMessage[0];
        String tt = splitMessage[1];
        String[] argsArray = splitMessage[2].split("\\| *(?=[^\\]]*?(?:\\[|$))");

        return makeMessage(tt, ap, argsArray);
    }

    private OSCMessage normalMessage(String s) throws Exception {
        //System.out.println("Reading as normal message: " + s);
        String[] message = s.split(",");
        String ap = message[0];
        String tt = message[1];
        if (message.length > 2) {
            String[] argsArray = message[2].split("\\|");
            return makeMessage(tt, ap, argsArray);
        }
        return makeMessage(tt, ap);
    }

    private OSCMessage makeMessage(String tt, String ap) {
        OSCMessageBuilder builder = new OSCMessageBuilder(this, ap);
        char[] ttArray = tt.toCharArray();
        for (int i = 0; i < ttArray.length; i++) {
            switch (ttArray[i]) {
                case 'T':
                    builder.add(Boolean.TRUE);
                    break;
                case 'F':
                    builder.add(Boolean.FALSE);
                    break;
                case 'N':
                    builder.addNull();
                    break;
            }
        }
        return builder.finalise();
    }

    private OSCMessage makeMessage(String tt, String ap, String[] argsArray) throws Exception {
        /*if(tt.contains(",")){
            tt = tt.substring(1);
        }*/
        char[] ttArray = tt.toCharArray(); //there is no comma
        ArrayList<String> args = new ArrayList<>();
        if (argsArray.length > 0) {
            for (String temp : argsArray) {
                args.add(temp);
            }
        }

        OSCMessageBuilder builder = new OSCMessageBuilder(this, ap);
        for (char c : ttArray) {
            switch (c) {
                case 'i':
                    isArgsEmpty(tt, ap, argsArray, args, c);
                    builder.add(Integer.parseInt(args.remove(0)));
                    break;
                case 'f':
                    isArgsEmpty(tt, ap, argsArray, args, c);
                    builder.add(Float.parseFloat(args.remove(0)));
                    break;
                case 's':
                    isArgsEmpty(tt, ap, argsArray, args, c);
                    String test = args.remove(0);
                    //System.out.println("adding: " + test);
                    builder.add(test);
                    break;
                case 'b':
                    isArgsEmpty(tt, ap, argsArray, args, c);
                    String blob = args.remove(0);
                    String[] blobArray = blob.split(".");
                    int[] blobInts = new int[blobArray.length];
                    for (int i = 0; i < blobArray.length; i++) {
                        blobInts[i] = Integer.parseInt(blobArray[i]);
                    }
                    builder.add(blobInts);
                    break;
                case 'T':
                    builder.add(Boolean.TRUE);
                    break;
                case 'F':
                    builder.add(Boolean.FALSE);
                    break;
                case 'N':
                    builder.addNull();
                    break;
                case 'I':
                    builder.addImpulse();
                    break;

            }
        }
        return builder.finalise();
    }

    private void isArgsEmpty(String tt, String ap, String[] argsArray, ArrayList<String> args, char c) throws Exception {
        if (args.isEmpty()) {
            throw new Exception("Expected more arguments in the message: "
                    + ap + tt + " " + Arrays.toString(argsArray)
                    + "\n Am on argument " + c);
        }
    }
}
