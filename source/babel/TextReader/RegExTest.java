package babel.TextReader;

import java.util.regex.*;

/**
 *
 * @author tomrbland
 */
public class RegExTest {

    /*public static void main(String args[]) {
        // String to be scanned to find the pattern.
        String line = "/translation,ss,[/dmx/0/<1:i:F:-1:0-31>,i,<2:i:LIN:0:0-255>]|[/ch/<1:i:F:+1:1-32>/mix/fader,f,<2:f:LIN:0:0-1>]";
        String pattern = "(\\[.*\\])()";
        Pattern r = Pattern.compile(pattern);
        System.out.println(line.replaceAll(pattern, "$2"));
        Matcher m = r.matcher(line);
        while (m.find()) {
            for (int i = 0; i < m.groupCount(); i++) {
                System.out.println("I found the text "
                        + m.group(i) + " starting at "
                        + "index " + m.start(i) + " and ending at index " + m.end(i));
            }
        }
    }*/

    /**
     *
     * @param args
     */

    
    public static void main(String args[]) {
        // String to be scanned to find the pattern.
        String line = "[/dmx/0/<1:i:F:-1:0-31>,i,<2:i:LIN:0:0-255>][/dmx/3/<1:i:F:-1:0-31>,i,<2:i:LIN:0:0-255>]";
        String pattern = "\\[(.*)\\]\\[(.*)\\]()";
        Pattern r = Pattern.compile(pattern);
        System.out.println(line.replaceAll(pattern, "$1"));
        Matcher m = r.matcher(line);
        while (m.find()) {
            for (int i = 0; i < m.groupCount(); i++) {
                System.out.println("I found the text "
                        + m.group(i) + " starting at "
                        + "index " + m.start(i) + " and ending at index " + m.end(i));
            }
        }
    }
}
