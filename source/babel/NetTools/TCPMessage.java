package babel.NetTools;

/**
 * Extends Message.
 *
 * @author T R Bland
 */
public class TCPMessage {

    private final TCPconnection connection;
    private final String sourceIP;
    private final int initialPort;
    private final int[] data;

    /**
     *
     * @param data
     * @param connection
     * @param sourceIP
     * @param initialPort
     */
    public TCPMessage(int[] data, TCPconnection connection, String sourceIP, int initialPort) {
        this.data = data;
        this.connection = connection;
        this.sourceIP = sourceIP;
        this.initialPort = initialPort;
    }
    
    /**
     *
     * @return get data
     */
    public int[] data(){
        return data;
    }

    /**
     *
     * @return get tcp connection
     */
    public TCPconnection connection() {
        return connection;
    }

    /**
     *
     * @return get source ip
     */
    public String sourceIP() {
        return sourceIP;
    }

    /**
     *
     * @return get the inital port connected on
     */
    public int initialPort() {
        return initialPort;
    }

}
