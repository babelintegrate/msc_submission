package babel.NetTools;

import babel.ArtNet.ArtNetRx;
import babel.Hub.Hub;
import babel.OSC.OSCMessage;
import babel.OSC.OSCtcpClient;
import babel.OSC.OSCtcpServer;
import babel.OSC.OSCudpServer;
import babel.TMS.TMS;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * For controlling messages to all NetTools and storing instances of various
 * NetTools
 *
 * @author T R Bland
 */
public class NetTools extends Thread {

    private final AtomicBoolean running = new AtomicBoolean(false);
    private final Hub hub = Hub.get();
    private final TMS messagesFromHub;

    /**
     * Master NetTools Constructor
     */
    public NetTools() {
        super("NetTools");
        new SystemInfo();
        messagesFromHub = hub.register();
    }

    @Override
    public void run() {
        running.set(true);
        while (running.get()) {
            if (messagesFromHub.check()) {
                try {
                    OSCMessage m = messagesFromHub.get();
                    if (m.addressPattern().startsWith("/system/")) {
                        decode(m);
                    }
                } catch (Exception ex) {
                    System.out.println("NET: Badly formed message: " + ex.toString());
                }
            }
            sleep();
        }
    }

    private void sleep() {
        try {
            Thread.sleep(0,1);
        } catch (InterruptedException e) {
            System.out.println("Exception in Pinger" + e.getLocalizedMessage());
        }
    }

    // /system/
    private void decode(OSCMessage m) throws Exception {
        String[] address = m.addressPattern().toLowerCase().split("/");
        switch (address[2]) {
            case "osc":
                systemOSC(address, m);
                break;
            case "artnet":
                systemARTNET(address, m);
                break;
        }
    }

    // /system/osc
    private void systemOSC(String[] address, OSCMessage m) {
        switch (address[3]) {
            case "server":
                decodeOSCServer(address, m);
                break;
            case "client":
                decodeOSCClient(address, m);
                break;
            default:
                System.err.println("NetTools.Decode - unknown " + Arrays.toString(address));
        }
    }

    // /system/artnet
    private void systemARTNET(String[] address, OSCMessage m) {
        switch (address[3]) {
            case "node":
                decodeArtNetNode(address, m);
                break;
            case "controller":
                System.err.println("Art-Net controllers are not yet implemented");
                break;
            default:
                System.err.println("NetTools.Decode - unknown " + Arrays.toString(address));
        }
    }

    // /system/osc/server/
    private void decodeOSCServer(String[] address, OSCMessage m) {
        switch (address[4]) {
            case "add":
            case "new":
                switch (address[5]) {
                    case "udp":
                        addUDPOSCserver(m);
                        break;
                    case "tcp":
                        addTCPOSCserver(m);
                        break;
                    default:
                        System.err.println("NetTools.decodeOSCServer unknown server type: " + Arrays.toString(address));
                }
                break;
            case "delete":
            case "remove":
                switch (address[5]) {
                    case "udp":
                        //removeUDPOSCserver(m);
                        break;
                    case "tcp":
                        //removeTCPOSCserver(m);
                        break;
                    default:
                        System.err.println("NetTools.decodeOSCServer unknown server type: " + Arrays.toString(address));
                }
                break;
        }
    }

    // /system/osc/client/    
    private void decodeOSCClient(String[] address, OSCMessage m) {
        switch (address[4]) {
            case "add":
            case "new":
                switch (address[5]) {
                    case "udp":
                        break;
                    case "tcp":
                        addTCPOSCclient(m);
                        break;
                    default:
                        System.err.println("NetTools.decodeOSCServer unknown server type: " + Arrays.toString(address));
                }
                break;
            case "delete":
            case "remove":
                switch (address[5]) {
                    case "udp":
                        break;
                    case "tcp":
                        //removeTCPOSCclient(m);
                        break;
                    default:
                        System.err.println("NetTools.decodeOSCServer unknown server type: " + Arrays.toString(address));
                }
                break;
        }
    }

    // /system/osc/server/add/udp
    private final ArrayList<OSCudpServer> oscUDPServers = new ArrayList<>();

    private void addUDPOSCserver(OSCMessage m) {
        if (!(m.typeTags().equals(",Tsi") || m.typeTags().equals(",Fsi"))) {
            return;
        }
        char[] tt = m.typeTags().toCharArray();
        String[] stringAddr = m.getString(0).split("\\.");
        if (stringAddr.length > 4 || stringAddr.length == 0) {
            System.err.println("NetTools.addUDPOSCserver Invalid Address: " + Arrays.toString(stringAddr));
            return;
        }
        byte[] ip = new byte[4];
        for (int i = 0; i < stringAddr.length; i++) {
            ip[i] = (byte) Integer.parseInt(stringAddr[i]);
        }
        try {
            InetSocketAddress addr = new InetSocketAddress(InetAddress.getByAddress(ip), m.getInt(1));
            OSCudpServer udp;
            switch (tt[1]) {
                case 'T':
                    udp = new OSCudpServer(true, true, addr);
                    udp.start();
                    oscUDPServers.add(udp);
                    break;
                case 'F':
                    udp = new OSCudpServer(true, false, addr);
                    udp.start();
                    oscUDPServers.add(udp);
                    break;
                default:
                    System.err.println("NetTools.addUDPOSCserver incorrect typetag: " + Arrays.toString(tt));
            }
        } catch (UnknownHostException ex) {
            System.err.println("NetTools.addUDPOSCserver Unknown host. " + ex.getLocalizedMessage());
        } catch (SocketException ex) {
            System.err.println("NetTools.addUDPOSCserver Socket Exception. " + ex.getLocalizedMessage());
        }
    }

    // /system/osc/server/add/tcp
    private final ArrayList<OSCtcpServer> oscTCPServers = new ArrayList<>();

    private void addTCPOSCserver(OSCMessage m) {
        if (!(m.typeTags().equals(",Ti") || m.typeTags().equals(",Fi"))) {
            return;
        }
        OSCtcpServer tcp;
        switch (m.typeTags().toCharArray()[0]) {
            case 'T':
                tcp = new OSCtcpServer(true, true, m.getInt(0));
                tcp.start();
                oscTCPServers.add(tcp);
                break;
            case 'F':
                tcp = new OSCtcpServer(true, false, m.getInt(0));
                tcp.start();
                oscTCPServers.add(tcp);
                break;
            default:
                System.err.println("NetTools.addUDPOSCserver incorrect typetag: " + m.typeTags());
        }
    }

    // /system/osc/client/add/tcp,<Boolean allowFeedback>si "ip.ip.ip.ip" <port>
    private final ArrayList<OSCtcpClient> oscTCPClients = new ArrayList<>();

    private void addTCPOSCclient(OSCMessage m) {
        if (!(m.typeTags().equals(",Ti") || m.typeTags().equals(",Fi"))) {
            return;
        }
        OSCtcpClient tcp;
        try {
            switch (m.typeTags().toCharArray()[0]) {
                case 'T':
                    tcp = new OSCtcpClient(true, true, m.getString(0), m.getInt(1));
                    tcp.start();
                    oscTCPClients.add(tcp);
                    break;
                case 'F':
                    tcp = new OSCtcpClient(true, false, m.getString(0), m.getInt(1));
                    oscTCPClients.add(tcp);
                    tcp.start();
                    break;
                default:
                    System.err.println("NetTools.addUDPOSCserver incorrect typetag: " + m.typeTags());
            }
        } catch (Exception ex) {
            System.err.println("Can't crete OSCtcpClient: " + ex.toString());
        }
    }

    // /system/artnet/node
    private void decodeArtNetNode(String[] address, OSCMessage m) {
        switch (address[4]) {
            case "start": // /system/artnet/node/start,N
                artnetNodeStart(m);
                break;
            case "messageperchannel":
                artnetEnableMessagePerChannel(m);
                break;
            case "onlysendchanges":
                artnetOnlySendChanges(m);
                break;
            case "data":
                artnetData(m);
                break;
            default:
                System.err.println("NetTools.Decode - unknown " + Arrays.toString(address));
        }
    }

    // /system/artnet/node/start
    private ArtNetRx artnetRX = null;

    private void artnetNodeStart(OSCMessage m) {
        if (!m.typeTags().equals(",N")) {
            return;
        }
        try {
            artnetRX = new ArtNetRx();
            artnetRX.start();
        } catch (Exception ex) {
            System.err.println("Can't start Artnet");
        }
    }

    // /system/artnet/node/messagePerChannel
    private void artnetEnableMessagePerChannel(OSCMessage m) {
        if (artnetRX == null) {
            System.err.println("Cant change state of artnetRX, it has not been started");
            return;
        }
        char[] tt = m.typeTags().toCharArray();
        if (tt[1] == 'T') {
            artnetRX.enableMessagePerChannel(true);
        } else if (tt[1] == 'F') {
            artnetRX.enableMessagePerChannel(false);
        }
    }

    // /system/artnet/node/onlySendChanges
    private void artnetOnlySendChanges(OSCMessage m) {
        if (artnetRX == null) {
            System.err.println("Cant change state of artnetRX, it has not been started");
            return;
        }
        char[] tt = m.typeTags().toCharArray();
        if (tt[1] == 'T') {
            artnetRX.onlySendChanges(true);
        } else if (tt[1] == 'F') {
            artnetRX.onlySendChanges(false);
        }
    }

    private void artnetData(OSCMessage m) {
        if (artnetRX == null) {
            System.err.println("Cant send artnetRX data, it has not been started");
            return;
        }
        artnetRX.sendData();
    }

    /**
     *
     */
    public void shutdown() {
        running.set(false);
        if (artnetRX != null) {
            artnetRX.shutdown();
        }
        for (OSCtcpServer t : oscTCPServers) {
            t.shutdown();
        }
        for (OSCudpServer t : oscUDPServers) {
            t.shutdown();
        }
        for (OSCtcpClient t : oscTCPClients) {
            t.shutdown();
        }
    }
}
