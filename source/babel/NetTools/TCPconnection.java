package babel.NetTools;

import babel.TMS.TypelessTMS;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * A TCP Connection
 *
 * @author T R Bland
 */
public class TCPconnection extends Thread {

    private Socket socket;
    private InputStream inputStream;
    private OutputStream outputStream;
    private AtomicBoolean connected = new AtomicBoolean(false);
    private final TypelessTMS messages;
    private final boolean client;

    //Useful info
    private String address;
    private InetAddress inetAddr;
    private int port;

    /*I would like all of the above to be final. But because assignement is
      inside a try. Compiler thinks that they may not have been initialised*/
    /**
     * A new TCP connection. Created by TCPserver.
     *
     * @param socket The socket to be used.
     * @param initialPort Port connection was made on.
     * @param timeout Socket timeout value
     * @param messages where to send messages to
     */
    public TCPconnection(Socket socket, int initialPort, int timeout, TypelessTMS messages) {
        super("TCPconnection");
        this.socket = socket;
        this.messages = messages;
        try {
            outputStream = socket.getOutputStream();
            inputStream = socket.getInputStream();
            address = socket.getInetAddress().getHostAddress();
            inetAddr = socket.getInetAddress();
            port = socket.getPort();
            this.port = initialPort;
            socket.setSoTimeout(timeout);
        } catch (IOException e) {
            System.err.println("TCPSocketThread error... " + e.getLocalizedMessage());
        }
        client = false;
    }

    /**
     *
     * @param address of desitination
     * @param port of destinaton
     * @param messages where to send messages to
     */
    public TCPconnection(InetAddress address, int port, TypelessTMS messages) {
        this.inetAddr = address;
        this.address = address.getHostAddress();
        this.port = port;
        this.messages = messages;
        client = true;
    }

    /**
     * Start the connection
     */
    @Override
    public void run() {
        if (client) {
            runClient();
        } else {
            connected.set(true);
            while (connected.get()) {
                listen();
                sleep();
            }
        }
        try {
            close();
        } catch (Exception e) {
            System.err.println("Could not close socket... " + e.getLocalizedMessage());
        }

    }

    private void runClient() {
        try {
            socket = new Socket(address, port);
            inputStream = socket.getInputStream();
            outputStream = socket.getOutputStream();
            connected.set(true);

            while (connected.get()) {
                listen();
                sleep();
            }

        } catch (IOException ex) {
            System.out.println("bugger");
        }
    }

    private void sleep() {
        try {
            Thread.sleep(0, 1);
        } catch (InterruptedException e) {
            System.out.println("Exception in Pinger");
        }
    }

    /**
     * Listen for incoming messages
     */
    private void listen() {
        try {
            if (inputStream.available() > 0) {
                readFromInputStream();
            }
        } catch (SocketTimeoutException e) {
            System.err.println("Socket timed out: " + e.getLocalizedMessage());
            System.err.println(e.toString());
            connected.set(false);
        } catch (IOException e) {
            System.err.println("IO exception on TCP socket: " + e.getLocalizedMessage());
            connected.set(false);
        } catch (Exception ex) {
            System.err.println("Tried to create invalid message. " + ex.getLocalizedMessage());
        }
    }

    /**
     * Used in listen() reads the input stream
     *
     * @throws IOException
     * @throws Exception
     */
    private void readFromInputStream() throws IOException, Exception {
        ArrayList<Integer> message = new ArrayList<>();
        while (inputStream.available() > 0) {
            message.add(getInput());
        }
        //outputStream.flush(); //Just for echo
        //System.out.println("\033[35m" + new java.util.Date() + " - TCP Rx: " + address + ":" + initialPort + " - " + message + "\033[0m");
        sendAsTCPMessage(message);
    }

    /**
     * gets byte from input
     *
     * @param message
     * @throws IOException
     */
    private int getInput() throws IOException {
        int c = inputStream.read();
        //outputStream.write(c); //Just for echo
        return c;
    }

    private void sendAsTCPMessage(ArrayList<Integer> message) throws Exception {
        //System.out.println("TCPconnection inputStream: " + message);
        TCPMessage m;
        m = new TCPMessage(serialize(message), this, address, port);
        messages.add(m);
    }

    private static int[] serialize(ArrayList<Integer> integers) throws IOException {
        int[] ret = new int[integers.size()];
        Iterator<Integer> iterator = integers.iterator();
        for (int i = 0; i < ret.length; i++) {
            ret[i] = iterator.next();
        }
        return ret;
    }

    /**
     * Close the connection
     *
     * @throws Exception
     */
    @SuppressWarnings("empty-statement")
    private void close() throws Exception {
        System.out.println("\033[32m" + new java.util.Date()
                + " - Closed TCP Connection: " + address()
                + ":" + port + "\033[0m");
        //tx.close(); CAUSES AN EXCEPTION?
        socket.shutdownOutput();// Sends the 'FIN' on the network
        while (inputStream.read() > 0);// "read()" returns '-1' when the 'FIN' is reached
        socket.close();// Now we can close the Socket
        inputStream.close();
    }

    /**
     *
     */
    public void shutdown() {
        connected.set(false);
    }

    //utilities
    /**
     *
     * @return address of the connection
     */
    public String address() {
        return address;
    }

    /**
     *
     * @param m integer array to send
     */
    public void send(int[] m) {
        try {
            //System.out.println("\033[35m" + new java.util.Date() + " - TCP Tx: " + address + ":" + initialPort + " - " + Arrays.toString(m) + "\033[0m");
            for (int i : m) {
                outputStream.write(i);
            }
        } catch (Exception ex) {
            System.err.println("TCPconnection can't send. " + ex.getLocalizedMessage());
        }
        //System.err.println("TCPconnection: CANT SEND AT THE MOMENT");
    }

}
