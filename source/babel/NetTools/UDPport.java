package babel.NetTools;

import babel.TMS.TypelessTMS;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 *
 * @author T R Bland
 */
public class UDPport extends Thread {

    private final AtomicBoolean running = new AtomicBoolean(false);

    private final DatagramSocket socket;
    private final int port;
    private final TypelessTMS tx;
    private final TypelessTMS messages;
    private final int bufferSize;

    private final boolean enableTx;
    private final boolean enableRx;

    /**
     *
     * @param port
     * @param bufferSize
     * @param messages
     * @param txAndRx if tx and rx from the same socket
     * @throws SocketException
     */
    public UDPport(int port, int bufferSize, TypelessTMS messages, boolean txAndRx) throws SocketException {
        super("UDP port: " + port);
        this.port = port;
        this.socket = new DatagramSocket(port);
        this.socket.setSoTimeout(1);
        this.bufferSize = bufferSize;
        this.messages = messages;
        this.tx = new TypelessTMS();
        enableTx = txAndRx;
        enableRx = true;
    }

    /**
     *
     * @param port
     * @throws SocketException
     */
    public UDPport(int port) throws SocketException {
        super("UDP port: " + port);
        this.port = port;
        this.socket = new DatagramSocket(port);
        this.socket.setSoTimeout(1);
        this.bufferSize = 0;
        this.messages = null;
        this.tx = new TypelessTMS();
        enableTx = true;
        enableRx = false;
    }

    private void print(byte[] bytes) {
        for (byte b : bytes) {
            System.out.print(Integer.toHexString((b & 0xFF)) + ", ");
        }
    }
    
    @Override
    public void run() {
        running.set(true);
        while (running.get()) {
            try {

                if (enableTx && tx.check()) {
                    DatagramPacket p = (DatagramPacket) tx.get();
                    socket.send(p);
                    //System.out.println("\033[35m" + new java.util.Date() + " - UDP Tx: " + p.getAddress() + ":" + p.getPort() + "\033[0m");
                }

                if (enableRx) {
                    byte[] buf = new byte[bufferSize];
                    DatagramPacket packet = new DatagramPacket(buf, buf.length);
                    socket.receive(packet);
                    int length = packet.getLength();
                    byte[] data  = new byte[length];
                    byte [] rawData = packet.getData();
                    for(int i = 0; i < length; i++){
                        data[i] = rawData[i];
                    }                    
                    packet.setData(data);
                    messages.add(packet);
                }
                sleep();
            } catch (SocketTimeoutException et) {
            } catch (IOException e) {
                System.err.println("UDP: Can't recieve packet from socket");
            } catch (Exception ex) {
                System.err.println("UDP: Can't read packet from TMS");
            }
        }
        socket.close();
        System.out.println("UDP Thread Closed: " + port);
    }

    private void sleep() {
        try {
            Thread.sleep(0, 1);
        } catch (InterruptedException e) {
            System.out.println("Exception in UDP port");
        }
    }

    /**
     *
     * @param p The DatagramPacket includes information indicating the data to
     * be sent, its length, the IP address of the remote host, and the port
     * number on the remote host.
     */
    public void send(DatagramPacket p) {
        tx.add(p);
    }

    /**
     * Things added to tx must be of type DatagramPacket
     *
     * @return
     */
    public TypelessTMS tms() {
        return tx;
    }

    /**
     *
     */
    public void shutdown() {
        running.set(false);
    }

    /**
     *
     * @return
     */
    public int port(){
        return socket.getLocalPort();
    }
}
