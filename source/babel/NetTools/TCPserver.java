package babel.NetTools;

import babel.TMS.TypelessTMS;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * TCP Server. Starts new Threads of TCPconnection when a connection is made.
 *
 * @author T R Bland
 */
public class TCPserver extends Thread {

    private final int port;
    
    //HOW TO REMOVE FROM THIS IF CONNECTION CLOSES?
    private final ArrayList<TCPconnection> connections;

    private ServerSocket serverSocket;
    private final AtomicBoolean running = new AtomicBoolean(false);
    
    /**
     *
     */
    public final TypelessTMS messages = new TypelessTMS();

    /**
     * Creates a TCP Server listening on the given port
     *
     * @param port
     */
    public TCPserver(int port) {
        super("TCPserver " + port);
        this.port = port;
        connections = new ArrayList<>();
    }

    /**
     * Boot the server
     */
    @Override
    public void run() {
        running.set(true);
        try {
            serverSocket = new ServerSocket(port);
            while (running.get()) {
                TCPconnection tcpc = new TCPconnection(serverSocket.accept(), port, 10000, messages);
                tcpc.start();
                System.out.println("\033[32m" + new java.util.Date()
                        + " - New TCP Connection: " + tcpc.address()
                        + ":" + port + "\033[0m");
                connections.add(tcpc);
            }
        } catch (IOException e) {
            System.out.println(new java.util.Date() + " - Exception caught when trying to listen on port " + port + " or listening for a connection");
            System.out.println(e.getMessage());
        }
    }

    /**
     * Close the server and its connections.
     */
    public void shutdown() {
        running.set(false);
        closeConnections();
        try {
            serverSocket.close();
        } catch (IOException ex) {
            System.out.println("Can't close TCPserver.");
        }
    }

    
    private void closeConnections() {
        for (TCPconnection t : connections) {
            try {
                t.shutdown();
            } catch (Exception e) {
                System.err.println("Cannot close connection");
                //t.stop();
            }
        }
    }

    /**
     *
     * @param m
     */
    public void sendMessage(int[] m) {
        if(connections.isEmpty()) {
            return;
        }
        for (TCPconnection t : connections){
            t.send(m);
        }
    }

    /**
     *
     * @param m
     */
    public void close(TCPMessage m) {
        TCPconnection t = m.connection();
        connections.remove(t);
        t.shutdown();
    }
}
