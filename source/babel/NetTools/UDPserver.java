package babel.NetTools;

import babel.TMS.TypelessTMS;
import java.net.DatagramPacket;
import java.net.InetSocketAddress;
import java.net.SocketException;

/**
 *
 * @author T R Bland
 */
public class UDPserver {

    private final int txPort;
    private final int rxPort;

    private final UDPport tx;
    private final UDPport rx;

    /**
     *
     */
    public final TypelessTMS messages = new TypelessTMS();

    /**
     *
     * @throws SocketException
     */
    public UDPserver() throws SocketException {
        this.txPort = 0;
        this.rxPort = 0;

        rx = new UDPport(rxPort, 1024, messages, true);
        tx = rx;
        System.out.println("\033[36m" + new java.util.Date()
                + " - New UDP Port: " + rx.port() + "\033[0m");
    }
    
    /**
     *
     * @param port
     * @throws SocketException
     */
    public UDPserver(int port) throws SocketException {
        this.txPort = port;
        this.rxPort = this.txPort;

        rx = new UDPport(rxPort, 1024, messages, true);
        tx = rx;
        System.out.println("\033[36m" + new java.util.Date()
                + " - New UDP Port: " + rx.port() + "\033[0m");
    }

    /**
     *
     * @param port
     * @param type
     * @throws SocketException
     */
    public UDPserver(int port, String type) throws SocketException {
        int bufferSize = 4026;
        switch (type) {
            case "tx":
                this.txPort = port;
                this.rxPort = 0;
                tx = new UDPport(rxPort, bufferSize, messages, true);
                rx = null;
                break;
            case "rx":
                this.txPort = 0;
                this.rxPort = port;
                rx = new UDPport(rxPort, bufferSize, messages, true);
                tx = null;
                break;
            case "rxtx":
            case "txrx":
                this.txPort = 0;
                this.rxPort = port;
                rx = new UDPport(rxPort, bufferSize, messages, true);
                tx = rx;
                break;
            default:
                throw new SocketException("Incorrect usage of UDPserver()");
        }

        System.out.println("\033[36m" + new java.util.Date()
                + " - New UDP Port: " + rx.port() + "\033[0m");
    }

    /**
     *
     */
    public void start() {
        if (rx == tx) {
            rx.start();
        } else {
            if (tx != null) {
                tx.start();
            }
            if (rx != null) {
                rx.start();
            }
        }
    }

    /**
     * Things added to the TypelessTMS must be of type DatagramPacket
     *
     * @return TypelessTMS of type DatagramPacket
     */
    public TypelessTMS tms() {
        if (tx != null) {
            return tx.tms();
        } else {
            return null;
        }
    }

    /**
     *
     */
    public void shutdown() {
        if (txPort == rxPort) {
            rx.shutdown();
        } else {
            if (tx != null) {
                tx.shutdown();
            }
            if (rx != null) {
                rx.shutdown();
            }
        }
    }

    /**
     *
     * @param stream
     * @param sa
     */
    public void sendMessage(int[] stream, InetSocketAddress sa) {
        if (tx == null) {
            return;
        }
        byte b[] = new byte[stream.length];
        for (int i = 0; i < stream.length; i++) {
            b[i] = (byte) stream[i];
        }
        DatagramPacket p = new DatagramPacket(b, b.length, sa.getAddress(), sa.getPort());
        tx.send(p);
    }
    
    /**
     *
     * @param stream
     * @param sa
     */
    public void sendMessage(byte[] stream, InetSocketAddress sa) {
        if (tx == null) {
            return;
        }
        DatagramPacket p = new DatagramPacket(stream, stream.length, sa.getAddress(), sa.getPort());
        tx.send(p);
    }
}
