package babel.NetTools;

import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author T R Bland
 */
public final class SystemInfo {

    /**
     *
     */
    public static final ArrayList<InetAddress> broadcastAddresses = new ArrayList<>();

    /**
     *
     */
    public static final ArrayList<InetAddress> ipAddresses = new ArrayList<>();

    /**
     *
     */
    public SystemInfo() {
        System.setProperty("java.net.preferIPv4Stack", "true"); //USE IPV4. Needed for auto detecting broadcast addresses
        updateBroadcastAddresses();
        getMyIpAddress();
    }

    /**
     *
     */
    public void updateBroadcastAddresses() {
        try {
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();

            while (interfaces.hasMoreElements()) {
                NetworkInterface networkInterface = interfaces.nextElement();
                if (networkInterface.isLoopback()) {
                    continue;    // Don't want to broadcast to the loopback interface
                }
                for (InterfaceAddress interfaceAddress : networkInterface.getInterfaceAddresses()) {
                    InetAddress broadcast = interfaceAddress.getBroadcast();
                    if (broadcast == null) {
                        continue;
                    }
                    broadcastAddresses.add(broadcast);
                }
            }

        } catch (SocketException ex) {
            Logger.getLogger(SystemInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void getMyIpAddress() {
        try {
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();

            while (interfaces.hasMoreElements()) {
                NetworkInterface networkInterface = interfaces.nextElement();
                if (networkInterface.isLoopback()) {
                    continue;    // Don't want to broadcast to the loopback interface
                }
                for (InterfaceAddress interfaceAddress : networkInterface.getInterfaceAddresses()) {
                    InetAddress addr = interfaceAddress.getAddress();
                    if (addr == null) {
                        continue;
                    }
                    ipAddresses.add(addr);
                }
            }

        } catch (SocketException ex) {
            Logger.getLogger(SystemInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
