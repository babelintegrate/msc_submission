package babel;

import babel.Controller.Controller;
import babel.Hub.Hub;
import babel.Midi.Midi;
import babel.NetTools.NetTools;
import babel.OSC.OSCMessage;
import babel.OSC.Type;
import babel.Pinger.Pinger;
import babel.TextReader.TextReader;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <p>
 * Main Babel application. Port 2323 is the port for control messages to setup
 * and control Babel.</p>
 *
 * <p>
 * Development notes: </p>
 * <ul>
 * <li>Use this for OCA? https://github.com/jmdns/jmdns register a service:
 * _oca._tcp </li>
 * <li>Web frontend: http://getuikit.com/docs/core.html </li>
 * <li>Temperature measure: vcgencmd measure_temp</li>
 * </ul>
 *
 * @author T R Bland
 */
public class Babel {

    /**
     *
     */
    public static final byte[] VERSION = {0x00, 0x01};
    private final Hub hub;
    private final boolean noGUI;
    private final AtomicBoolean running = new AtomicBoolean(true);
    private int exitStatus = 100;

    /**
     * Master Babel constructor
     *
     * @param args If run with args == "noGUI" then application will run
     * normally. Otherwise application will run the process "startx firefox".
     * This is to be used when the system is setup to run firefox in kiosk mode.
     */
    public Babel(String[] args) {
        noGUI = args.length > 0;
        hub = Hub.get();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Babel b = new Babel(args);
        System.exit(b.go());
    }

    private int go() {
        //Boot Message Distribution Center
        hub.babel(this);
        hub.start();

        //boot internals
        NetTools netTools = new NetTools();
        netTools.start();

        //boot externals        
        Pinger pinger = new Pinger();
        pinger.start();

        Midi midi = new Midi();
        midi.start();

        //Load Settings
        loadSettings();

        //boot control
        Controller controller = new Controller();
        controller.start();

        Process webserver = null;
        try {
            webserver = bootWebserver();
            //Runtime.getRuntime().exec("./launchWebserver");
        } catch (IOException ex) {
            System.err.println("webserver: " + ex.getLocalizedMessage());
        }

        Process gui = null;
        if (!noGUI) {
            try {
                gui = gui();
            } catch (IOException ex) {
                System.err.println("gui: " + ex.getLocalizedMessage());
            }
        }

        while (running.get()) {
            sleep(50);
        }
        hub.add(new OSCMessage(this, "/system/control/shuttingdown", Type.Null));
        controller.shutdown();
        midi.shutdown();
        pinger.shutdown();
        netTools.shutdown();
        sleep(1000);
        hub.shutdown();
        if (gui != null) {
            gui.destroy();
            if (gui.isAlive()) {
                System.err.println("GUI is still alive! trying to force exit");
                gui.destroyForcibly();
            }
        }
        if (webserver != null) {
            webserver.destroy();
        }
        return exitStatus;
    }

    private void sleep(int time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException ex) {
            System.out.println("Babel sleep interrupted");
        }
    }

    private Process bootWebserver() throws IOException {
        ProcessBuilder webserver = new ProcessBuilder("./launchWebserver");
        webserver.inheritIO();

        Process p;
        try {
            p = webserver.start();
            System.out.println("Started Webserver");
            return p;
        } catch (IOException ex) {
            throw new IOException("Can't start the server. " + ex.getLocalizedMessage());
        }
    }

    private Process gui() throws IOException {
        ProcessBuilder gui = new ProcessBuilder("startx", "firefox");
        gui.inheritIO();

        Process p;
        try {
            p = gui.start();
            return p;
        } catch (IOException ex) {
            throw new IOException("Can't start the gui. " + ex.getLocalizedMessage());
        }
    }

    private void loadSettings() {
        TextReader tr = TextReader.get();
        String file = "config.babel";
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            for (String line; (line = br.readLine()) != null;) {
                if ((line.startsWith("/system/")) && !line.contains(" ")) {
                    try {
                        hub.add(tr.read(line));
                    } catch (Exception ex) {
                        System.err.println("Error when reading config file: " + ex.getMessage());
                    }
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Babel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param exitStatus integer to set as return value. Used to dictate next
     * operation if appllication is run from bash script "run".
     */
    public void shutdown(int exitStatus) {
        running.set(false);
        this.exitStatus = exitStatus;
    }
}
