package babel.TMS;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Thread Messaging Service - for inter thread communication.
 *
 * @author T R Bland
 */
public class TypelessTMS {

    private final ConcurrentLinkedQueue<Object> things = new ConcurrentLinkedQueue<>();

    /**
     *
     * @param o
     * @return true if message was successfully added.
     */
    public boolean add(Object o) {
        return things.offer(o);
    }

    /**
     * Check if there is a message ready to be read.
     *
     * @return true if there is a message ready to read.
     */
    public boolean check() {
        return !things.isEmpty();
    }

    /**
     *
     * @return The oldest message to read
     * @throws Exception if there are no messages to get.
     */
    public Object get() throws Exception {
        return things.remove();
    }
    
    /**
     *
     * @return The oldest message to read. Does not remove from buffer
     */
    public Object peek() {
        return things.peek();
    }

    /**
     *
     */
    public void bin() {
        if(!things.isEmpty()) {
            things.remove();
        }
    }

}
