package babel.TMS;

import babel.OSC.OSCMessage;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Thread Messaging Service - for inter thread communication.
 *
 * @author T R Bland
 */
public class TMS {

    private final ConcurrentLinkedQueue<OSCMessage> messages = new ConcurrentLinkedQueue<>();

    /**
     *
     * @param m a Message
     * @return true if message was successfully added.
     */
    public boolean add(OSCMessage m) {
        return messages.offer(m);
    }

    /**
     * Check if there is a message ready to be read.
     *
     * @return true if there is a message ready to read.
     */
    public boolean check() {
        return !messages.isEmpty();
    }

    /**
     *
     * @return The oldest message to read
     * @throws Exception if there are no messages to get.
     */
    public OSCMessage get() throws Exception {
        return messages.remove();
    }
    
    /**
     *
     * @return The oldest message to read. Does not remove from buffer
     */
    public OSCMessage peek() {
        return messages.peek();
    }

    /**
     *
     */
    public void bin() {
        if(!messages.isEmpty()) {
            messages.remove();
        }
    }

}
