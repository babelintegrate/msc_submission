package babel;

import babel.Hub.Hub;
import babel.OSC.OSCMessage;
import babel.OSC.Type;

/**
 * Processes to run inorder to exit application
 *
 * @author T R Bland
 */
public class Shutdown extends Thread {

    private final Process webserver;

    Shutdown(Process webserver) {
        super("Shutdown");
        this.webserver = webserver;
    }

    @Override
    public void run() {
        OSCMessage m = new OSCMessage(this, "SHUTDOWN", Type.True);
        Hub.get().add(m);
        if (webserver != null) {
            webserver.destroy();
        }
    }

}
