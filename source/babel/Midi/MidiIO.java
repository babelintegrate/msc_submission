package babel.Midi;

import babel.OSC.OSCMessage;
import babel.OSC.OSCMessageBuilder;
import javax.sound.midi.MetaMessage;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.Receiver;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.SysexMessage;

/**
 *
 * @author tom
 */
public class MidiIO implements Receiver {

    private final Midi parent;
    
    /**
     *
     * @param parent
     */
    public MidiIO(Midi parent) {
        this.parent = parent;
    }

    //RECEIVER------------------------------------------------------------------

    /**
     *
     * @param m
     * @param message
     * @param timeStamp
     */
    public void send(OSCMessage m, MidiMessage message, long timeStamp) {
        if(m.source() != this){
            send(message, timeStamp);
        }
    }
    
    
    @Override
    public void send(MidiMessage message, long timeStamp) {
        OSCMessage m;
        if (message instanceof ShortMessage) {
            ShortMessage sm = (ShortMessage) message;
            if(message.getStatus() != ShortMessage.ACTIVE_SENSING && message.getStatus() != ShortMessage.TIMING_CLOCK){
                //System.out.println("Status: " + sm.getStatus() + " command: " + sm.getCommand() + " channel: " + sm.getChannel());
                m = decodeMessage((ShortMessage) message);
                parent.toHub(m);
            }
        } else if (message instanceof SysexMessage) {
            m = decodeMessage((SysexMessage) message);
            parent.toHub(m);
        } else if (message instanceof MetaMessage) {
            m = decodeMessage((MetaMessage) message);
            parent.toHub(m);
        } else {
            m = new OSCMessage(this, "/midi/error", "Unknown Midi Message Recieved");
            parent.toHub(m);
        }
        //System.out.println("" + m.textMessage());
        
    }

    @Override
    public void close() {
    }

    private OSCMessage decodeMessage(ShortMessage message) {        
        OSCMessageBuilder mb = new OSCMessageBuilder(this, "/midi/"+message.getChannel()+"/"+message.getCommand());
        mb.add(message.getData1());
        mb.add(message.getData2());
        return mb.finalise();
    }
    
    private OSCMessage decodeMessage(SysexMessage message) {
        return new OSCMessage(this, "/midi/error", "MIDI Sysex not supported");
    }
    
    private OSCMessage decodeMessage(MetaMessage message) {
        return new OSCMessage(this, "/midi/error", "MIDI Sysex not supported");
    }  
}
