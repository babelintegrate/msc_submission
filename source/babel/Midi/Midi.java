package babel.Midi;

import babel.Hub.Hub;
import babel.OSC.OSCMessage;
import babel.TMS.TMS;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Receiver;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Transmitter;

/**
 *
 * @author tom
 */
public class Midi extends Thread {

    private final AtomicBoolean running = new AtomicBoolean(false);
    private final TMS messagesFromHub;
    private final Hub hub = Hub.get();
    private final ArrayList<MidiDevice> devices = new ArrayList<>();
    private final ArrayList<Receiver> rx = new ArrayList<>();
    private final ArrayList<Transmitter> tx = new ArrayList<>();

    /**
     *
     */
    public Midi() {
        super("Midi");
        messagesFromHub = hub.register();
    }

    @Override
    public void run() {
        running.set(true);
        while (running.get()) {
            if (messagesFromHub.check()) {
                try {
                    OSCMessage m = messagesFromHub.get();
                    if (m.addressPattern().startsWith("/system/midi")) {
                        decode(m);
                    } else if (m.addressPattern().startsWith("/midi")) {
                        send(m);
                    }
                } catch (Exception ex) {
                    System.out.println("MIDI: Badly formed message: " + ex.toString());
                }
            }
            sleep();
        }
        closeDevices();
    }

    private void sleep() {
        try {
            Thread.sleep(0,1);
        } catch (InterruptedException e) {
            System.out.println("Exception in Pinger" + e.getLocalizedMessage());
        }
    }

    private void decode(OSCMessage m) {
        if (m.addressPattern().equals("/system/midi")) {
            switch (m.typeTags()) {
                case ",T":
                    getDevices();
                    openDevices();
                    getRecieversAndTransmitters();
                    wireUp();
                    break;
                case ",F":
                    closeDevices();
                    break;
            }
        }
    }

    private void getDevices() {
        MidiDevice.Info[] infos = MidiSystem.getMidiDeviceInfo();
        System.out.println("available devices: " + Arrays.toString(infos));

        for (MidiDevice.Info i : infos) {
            try {
                MidiDevice device = MidiSystem.getMidiDevice(i);
                if (device.getMaxTransmitters() != 0 || device.getMaxReceivers() != 0) {
                    devices.add(device);
                    //System.out.println("Found Midi Device: " + device.getDeviceInfo());
                }
            } catch (MidiUnavailableException ex) {
                System.out.println("Cant get midi device: " + i);
            }
        }
    }

    private void openDevices() {
        for (MidiDevice md : devices) {
            try {
                md.open();
            } catch (MidiUnavailableException ex) {
                System.err.println("Can't open MidiDevice: " + md.getDeviceInfo() + md.getMaxTransmitters());
            }
        }
    }

    private void closeDevices() {
        for (MidiDevice md : devices) {
            md.close();
        }
    }

    private void getRecieversAndTransmitters() {
        for (MidiDevice md : devices) {
            if (md.getMaxReceivers() < 0) {
                try {
                    rx.add(md.getReceiver());
                    System.out.println("Sending MIDI to: " + md.getDeviceInfo());
                } catch (MidiUnavailableException ex) {
                    System.out.println("Cant open midi port on: " + md.getDeviceInfo());
                }
            } else if (md.getMaxReceivers() > 0) {
                List<Receiver> lr = md.getReceivers();
                for (Receiver r : lr) {
                    rx.add(r);
                    System.out.println("Sending MIDI to: " + md.getDeviceInfo());
                }
            }

            if (md.getMaxTransmitters() < 0) {
                try {
                    tx.add(md.getTransmitter());
                    System.out.println("Listening for MIDI from: " + md.getDeviceInfo());
                } catch (MidiUnavailableException ex) {
                    System.out.println("Cant open midi port on: " + md.getDeviceInfo());
                }
            } else if (md.getMaxTransmitters() > 0) {
                List<Transmitter> lr = md.getTransmitters();
                for (Transmitter t : lr) {
                    tx.add(t);
                    System.out.println("Listening for MIDI from: " + md.getDeviceInfo());
                }
            }
        }
    }

    private void wireUp() {
        for (Transmitter t : tx) {
            t.setReceiver(new MidiIO(this));
        }
    }

    /**
     *
     */
    public void shutdown() {
        running.set(false);
    }

    void toHub(OSCMessage m) {
        hub.add(m);
    }

    // /midi
    void send(OSCMessage m) {
        int command, channel, data1, data2;
        String[] ap = m.addressPattern().split("/");
        channel = Integer.parseInt(ap[2]);
        command = Integer.parseInt(ap[3]);
        data1 = m.getInt(0);
        data2 = m.getInt(1);

        //System.out.println("midi message: " + channel + " " + command + " " + data1 + " " + data2);
        try {
            ShortMessage midi = new ShortMessage(command, channel, data1, data2);
            for (Receiver r : rx) {                
                r.send(midi, -1); //-1 indicates I don't care about timing. Need to stop feedback....
            }
        } catch (InvalidMidiDataException ex) {
            //System.err.println("Can't make MIDI message from: " + m.textMessage());
        }
    }
}
