/**
 * An extra feature, this is a nod towards life-safety systems. It pings specific addrss and sends OSC errors if an address does not respond.
 */
package babel.Pinger;
