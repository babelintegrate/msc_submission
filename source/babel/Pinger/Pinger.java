package babel.Pinger;

import babel.Hub.Hub;
import babel.OSC.OSCMessage;
import babel.OSC.OSCMessageBuilder;
import babel.TMS.TMS;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * The module for controlling pings. It watches the Hub wor Pinger messages
 *
 * @author T R Bland
 */
public class Pinger extends Thread {

    private final ArrayList<Ping> pings;
    private final AtomicBoolean running = new AtomicBoolean(false);
    private final Hub hub = Hub.get();
    private final TMS messagesFromHub;

    /**
     *
     */
    public Pinger() {
        super("Pinger");
        pings = new ArrayList<>();
        messagesFromHub = hub.register();
    }

    /**
     *
     * @param m message to send
     */
    public void send(OSCMessage m) {
        m.source(this);
        Hub.get().add(m);
    }

    @Override
    public void run() {
        running.set(true);
        while (running.get()) {
            if (messagesFromHub.check()) {
                try {
                    OSCMessage m = messagesFromHub.get();
                    if (m.addressPattern().startsWith("/system/")) {
                        decode(m);
                    }
                } catch (Exception ex) {
                    System.err.println("Pinger exception in run(): " + ex.getLocalizedMessage() + " \n\t" + ex.toString());
                }
            }
            sleep();
        }

    }

    private void sleep() {
        try {
            Thread.sleep(0,1);
        } catch (InterruptedException e) {
            System.err.println("Exception in Pinger");
        }
    }

    private void decode(OSCMessage m) throws Exception {
        if (m.source() == this) {
            return;
        }

        String[] address = m.addressPattern().toLowerCase().split("/");
        if (!address[2].equals("ping")) {
            return;
        }

        switch (address[3]) {
            case "status":
                status(m);
                break;
            case "new":
            case "add":
                newPing(m);
                break;
            case "stop":
            case "delete":
            case "remove":
                removePing(m);
                break;
            case "frequency":
                pingFrequency(m);
                break;
            case "confirms":
                pingConfirms(m);
                break;
            default:
                System.err.println("Unkown Pinger Message");
        }
    }

    private Ping findPing(byte[] ip) throws Exception {
        for (Ping p : pings) {
            if (Arrays.equals(ip, p.ip().getAddress())) {
                return p;
            }
        }
        throw new Exception("Pinger doesn't exist!");
    }

    private byte[] getIP(String[] data) throws NumberFormatException {
        byte[] ip = new byte[4];
        for (int i = 0; i < 4; i++) {
            ip[i] = (byte) Integer.parseInt(data[i]);
        }
        return ip;
    }

    // /system/ping/add,si "ip.ip.ip.ip" freq
    private void newPing(OSCMessage m) throws Exception {
        if (!",si".equals(m.typeTags())) {
            return;
        }
        String[] data = m.getString(0).split("\\.");
        //System.out.println("Pinger Message: " + Arrays.toString(data));

        try {
            byte[] ip = getIP(data);
            try {
                findPing(ip);
            } catch (Exception ex) {
                int frequency = m.getInt(1);
                Ping p = new Ping(this, ip, frequency);
                pings.add(p);
                p.start();
                OSCMessage reply = new OSCMessage(this, "system/message", "New pinger: " + p.ip().getHostAddress());
                send(reply);
                System.out.println("New pinger: " + p.ip().getHostAddress());
            }
        } catch (NumberFormatException nex) {
            throw new Exception("Ping: Malformed message: " + nex.getLocalizedMessage());
        }
    }

    // /system/ping/remove "ip.ip.ip.ip"
    private void removePing(OSCMessage m) throws Exception {
        if (!m.typeTags().equals(",s")) {
            return;
        }
        String[] data = m.getString(0).split("\\.");
        byte[] ip = getIP(data);
        Ping p = findPing(ip);
        p.shutdown();
        pings.remove(p);
        System.out.println("Pinger removed: " + p.ip().getHostAddress());
    }

    // /system/ping/frequency,si "ip.ip.ip.ip" freq
    // /system/ping/frequency,s "ip.ip.ip.ip"
    private void pingFrequency(OSCMessage m) throws Exception {
        if (!(m.typeTags().equals(",s") || m.typeTags().equals(",si"))) {
            return;
        }

        String[] data = m.getString(0).split("\\.");
        byte[] ip = getIP(data);
        Ping p = findPing(ip);

        if (m.typeTags().equals(",s")) {
            OSCMessageBuilder message = new OSCMessageBuilder(this, "ping/frequency");
            message.add(p.ip().getHostAddress());
            message.add(p.frequency());
            send(message.finalise());
        } else if (m.typeTags().equals(",si")) {
            p.frequency(m.getInt(1));
        }
    }

    //confirms,s<T/F> "ip.ip.ip.ip"
    //confirms,s "ip.ip.ip.ip"
    private void pingConfirms(OSCMessage m) throws Exception {
        if (!(m.typeTags().equals(",s") || m.typeTags().equals(",sT") || m.typeTags().equals(",sF"))) {
            return;
        }

        String[] data = m.getString(0).split("\\.");
        byte[] ip = getIP(data);
        Ping p = findPing(ip);

        switch (m.typeTags()) {
            case ",sT":
                p.confirms(true);
                break;
            case ",sF":
                p.confirms(false);
                break;
            case ",s":
                OSCMessageBuilder message = new OSCMessageBuilder(this, "/system/ping/confirms");
                message.add(p.ip().getHostAddress());
                message.add(p.confirms());
                send(message.finalise());
                break;
            default:
                System.err.println("Unkown pinger confirms message");
                break;
        }
    }

    // /system/ping/status,N
    private void status(OSCMessage m) {
        if (m.typeTags().equals(",N")) {
            sendStatus();
        }
    }

    private void sendStatus() {
        try {
            if (pings.isEmpty()) {
                sendEmptyStatus();
            } else {
                sendActivePings();
            }
        } catch (Exception ex) {
            System.err.println("Pinger.sendStatus: Cannot make message. " + ex.getLocalizedMessage());
        }
    }

    private void sendEmptyStatus() throws Exception {
        OSCMessage m = new OSCMessage(this, "/system/ping/status", "No active pings");
        send(m);
    }

    // replies: /system/ping/status,sisisi "192.168.2.12" 5 "192.168.2.10" 10
    private void sendActivePings() throws Exception {
        OSCMessageBuilder m = new OSCMessageBuilder(this, "/system/ping/status");

        for (Ping p : pings) {
            m.add(p.ip().getHostAddress());
            m.add(p.frequency());
        }

        OSCMessage message = m.finalise();
        send(message);
    }

    /**
     *
     */
    public void shutdown() {
        for(Ping p : pings){
            p.shutdown();
        }
        running.set(false);
    }
}
