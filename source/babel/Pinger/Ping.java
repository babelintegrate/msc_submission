package babel.Pinger;

import babel.OSC.OSCMessage;
import babel.OSC.OSCMessageBuilder;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * May need to do this? sudo chmod 4755 /bin/ping
 *
 *
 * Ping through list of IP addresses ever x seconds. Send error message if one
 * doesn't reply
 *
 * @author T R Bland
 */
public class Ping extends Thread {

    private final AtomicInteger frequency;
    private final InetAddress ip;
    private final AtomicBoolean running = new AtomicBoolean(false);
    private final Pinger parent;
    private final AtomicBoolean confirms = new AtomicBoolean(false);
    private boolean givenPermissionsWarning = false;
    private boolean useSystemCommand = false;
    private OSCMessage noResponseMessage = null;

    /**
     * @param parent to send messages to
     * @param ip destination IP address
     * @param frequency how often pings should be sent
     * @throws UnknownHostException if can't find host
     */
    public Ping(Pinger parent, String ip, int frequency) throws UnknownHostException {
        super("Ping ip");
        this.parent = parent;
        this.ip = InetAddress.getByName(ip);
        this.frequency = new AtomicInteger(frequency);
    }

    /**
     *
     * @param parent to send messages to
     * @param ip destination IP address
     * @param frequency how often pings should be sent
     * @throws UnknownHostException if can't find host
     */
    public Ping(Pinger parent, byte[] ip, int frequency) throws UnknownHostException {
        super("Ping ip");
        this.parent = parent;
        this.ip = InetAddress.getByAddress(ip);
        this.frequency = new AtomicInteger(frequency);
    }

    @Override
    public void run() {
        running.set(true);
        while (running.get()) {
            ping();
            sleep();
        }
    }

    private void ping() {
        try {
            final int timeout = 1, count = 1;
            if (useSystemCommand) {
                runSystemCommand("ping " + ip.getHostAddress() + " -c " + count + " -w " + timeout);
                if (confirms()) {
                    sendConfirmation(true);
                }
            } else {
                boolean connected = ip.isReachable(timeout);
                if (connected && confirms()) {
                    sendConfirmation(true);
                } else if (!connected) {
                    //try running terminal command (may not have permission for RAW ICMP socket)
                    useSystemCommand();
                }
            }
        } catch (Exception e) {
            sendErrorMessage();
        }
    }

    private void useSystemCommand() {
        useSystemCommand = true;
        if (!givenPermissionsWarning) {
            String message = "Don't have permission to access raw ICMP sockets "
                    + "and the device either does not exist or does not respond "
                    + "to TCP connection on socket 7 (echo). Try running Babel as su? "
                    + "Will use ping system command in future. This is a Debian specific "
                    + "command. It is not platform independant.";
            System.err.println(message);
            parent.send(new OSCMessage(this, "/system/ping/error", message));
            givenPermissionsWarning = true;
        }
    }

    private void sendErrorMessage() {
        if (noResponseMessage == null) {
            OSCMessageBuilder m = new OSCMessageBuilder(this, "/system/ping/error");
            m.add(ip.getHostAddress());
            m.add("no response");
            noResponseMessage = m.finalise();
        }
        if (confirms()) {
            sendConfirmation(false);
        }
        parent.send(noResponseMessage);
    }

    private void sendConfirmation(boolean state) {
        OSCMessageBuilder m = new OSCMessageBuilder(this, "/ping/confirm");
        m.add(ip.getHostAddress());
        m.add(state);
        parent.send(m.finalise());
    }

    private void runSystemCommand(String command) throws Exception {
        int i = 100;

        try {
            Process p = Runtime.getRuntime().exec(command);
            i = p.waitFor(); //Get exit status
        } catch (IOException | InterruptedException ex) {
            System.out.println("System Command Exception. " + ex.getLocalizedMessage());
        }

        if (i != 0) {
            throw new Exception("No reply from " + ip.getHostAddress());
        }
    }

    private void sleep() {
        try {
            Thread.sleep(frequency.get() * 1000);
        } catch (InterruptedException ex) {
            System.out.println("Exception in Pinger. " + ex.getLocalizedMessage());
        }
    }

    /**
     *
     * @return ipaddress being pinged
     */
    public InetAddress ip() {
        return ip;
    }

    /**
     *
     */
    public void shutdown() {
        running.set(false);
    }

    /**
     *
     * @return frequency of pings
     */
    public int frequency() {
        return frequency.get();
    }

    /**
     *
     * @param i frequency of pigs
     */
    public void frequency(int i) {
        frequency.set(i);
    }

    /**
     *
     * @param b should pinger send confirmation messages?
     */
    public void confirms(boolean b) {
        confirms.set(b);
    }

    /**
     *
     * @return is pinger sending confirmation messages?
     */
    public boolean confirms() {
        return confirms.get();
    }
}
