package babel.Hub;

/**
 *
 * @author tom
 */
public abstract class MediationArgument {

    final String rule;
    /*final int locationInxParts;*/
    String value = "";
    String index;

    /**
     *
     * @param rule
     */
    public MediationArgument(String rule) {
        if(rule != null){
        String r = rule;
        if(r.contains("<")){
            r = r.replaceAll("<", "");
            r = r.replaceAll(">", "");
        }
        this.rule = r;
        } else {
            this.rule = null;
        }
        //this.locationInxParts = locationInxParts;
    }
    
    //abstract public String value(String source, MediationArgument sourceArgument)  throws Exception ;

    /**
     *
     * @return
     */
    
    public String value() {
        return value;
    }
    
    String index(){
        return index;
    }
    
    /*int location() {
        return locationInxParts;
    }*/
}
