package babel.Hub;

/**
 *
 * @author tom
 */
public class ConstantMediationArgument extends MediationArgument{

    /**
     *
     * @param rule
     * @param index
     */
    public ConstantMediationArgument(String rule, String index) {
        super(rule);
        this.value = rule;
        this.index = "C" + index;
    }   
}
