package babel.Hub;

import babel.OSC.OSCMessage;
import babel.TextReader.TextReader;
import java.util.Timer;
import java.util.TimerTask;

/**
 * {@literal /messageTimer,si,[/osc/message/to/send,ii,1|2]|<int frequency>}
 *
 * @author tom
 */
public class MessageTimer {

    private final OSCMessage message;
    private final int frequency;
    private final Hub hub = Hub.get();
    private final Timer t;

    /**
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        TextReader tr = TextReader.get();
        MessageTimer mr = new MessageTimer(tr.read("/system/messagetimer,si,[/xremote,N]|9"));
    }

    /**
     *
     * @param m
     * @throws Exception
     */
    public MessageTimer(OSCMessage m) throws Exception {
        TextReader tr = TextReader.get();
        String temp = m.getString(0).substring(1);
        temp = temp.substring(0, temp.length() - 1);
        message = tr.read(temp);
        frequency = m.getInt(1);
        t = new Timer("MessageTimer: " + message.textMessage());
        final TimerTask task = new TimerTask() {
            @Override
            public void run() {
                hub.add(message);
            }
        };
        t.schedule(task, 100, frequency * 1000);
    }

    void shutdown() {
        t.cancel();
    }
    
   
}
