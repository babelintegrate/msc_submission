package babel.Hub;

import babel.OSC.OSCMessage;
import babel.TextReader.TextReader;

/**
 *
 * @author tomrbland
 */
public class Rule {

    private final Definition d1, d2;
    private final boolean bidirectional;

    /**
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        String translation, testMessage, aim;

        translation = "/system/rule,ssT,[/midi/0/176,ii,9|<1:i:0:LIN:0-127>]|[/fx/3/par/02,f,<1:f:0:LIN:0-1>]";
        testMessage = "/midi/0/176,ii,9|0";
        aim = "/fx/3/par/02,f,0.0";
        test(translation, testMessage, aim, 0.0);
        testMessage = "/fx/3/par/02,f,0.0";
        aim = "/midi/0/176,ii,9|0";
        test(translation, testMessage, aim, 0.1);
        testMessage = "/midi/0/176,ii,8|0";
        testToFail(translation, testMessage, 0.2);

        translation = "/system/rule,ssT,[/dmx/0/<1:i:0:LIN:0-31>,i,<2:i:0:LIN:0-255>]|[/ch/<1:i:0:LIN:1-32>/mix/fader,f,<2:f:0:LIN:0-1>]";
        testMessage = "/dmx/0/0,i,127";
        aim = "/ch/1/mix/fader,f,0.49803922";
        test(translation, testMessage, aim, 1.0);

        translation = "/system/rule,ssF,[/midi/0/176,ii,<1:i:0:LIN:0-31>|<2:i:0:LIN:0-127>]|[/ch/<1:i:2:LIN:1-32>/mix/fader,f,<2:f:0:LIN:0-1>]";
        testMessage = "/ch/1/mix/fader,f,1.0";
        aim = "/midi/0/176,ii,0|127";
        test(translation, testMessage, aim, 2.0);

        testMessage = "/midi/0/176,ii,0|0";
        aim = "/ch/01/mix/fader,f,0.0";
        test(translation, testMessage, aim, 2.1);

        translation = "/system/rule,ssT,[/midi/<1:i:0:LIN:0-4>/176,ii,<2:i:0:LIN:0-8>|<3:i:0:LIN:0-127>]|[/ch/<2:i:2:LIN:1-9>/mix/<1:i:2:LIN:1-4>/level,f,<3:f:0:LIN:0-1>]";
        testMessage = "/midi/0/176,ii,1|127";
        aim = "/ch/02/mix/01/level,f,1.0";
        test(translation, testMessage, aim, 3.0);

        translation = "/system/rule,ssT,[/midi/0/176,ii,9|127]|[/fx/3/par/02,f,1.0]";
        testMessage = "/midi/0/176,ii,9|127";
        aim = "/fx/3/par/02,f,1.0";
        test(translation, testMessage, aim, 4.0);

        testMessage = "/midi/0/176,ii,9|120";
        testToFail(translation, testMessage, 4.1);

    }

    /**
     *
     * @param translationSpec
     * @param testMessage
     * @param aim
     * @param id
     * @throws Exception
     */
    public static void test(String translationSpec, String testMessage, String aim, double id) throws Exception {
        TextReader tr = TextReader.get();
        OSCMessage m = tr.read(translationSpec);
        Rule r = new Rule(m);
        m = tr.read(testMessage); //should churn out an OSC message /ch
        m = r.mediate(m);
        String outcome = m.textMessage();
        if (outcome.equals(aim)) {
            //System.out.println("Passed " + id + "\n");
        } else {
            System.out.println("Failed " + id + ". "
                    + "\n\tFrom a testMessage of: " + testMessage
                    + "\n\tAiming for:            " + aim
                    + "\n\tGot:                   " + outcome
                    + "\n");
        }
    }

    /**
     *
     * @param translationSpec
     * @param testMessage
     * @param id
     * @throws Exception
     */
    public static void testToFail(String translationSpec, String testMessage, double id) throws Exception {
        TextReader tr = TextReader.get();
        OSCMessage m = tr.read(translationSpec);
        Rule r = new Rule(m);
        m = tr.read(testMessage); //should churn out an OSC message /ch
        try {
            m = r.mediate(m);
            String outcome = m.textMessage();
            System.out.println("Failed " + id + ". "
                    + "\n\tFrom a testMessage of: " + testMessage
                    + "\n\tAiming for nothing     "
                    + "\n\tGot:                   " + outcome
                    + "\n");
        } catch (Exception ex) {
        }
    }

    /**
     *
     * @param m
     * @throws Exception
     */
    public Rule(OSCMessage m) throws Exception {
        String part1, part2;
        String message = m.getDataAsString();

        //Remove [][] all at once rather than getting each string and removing [] on each string seperately
        String pattern = "\\[(.*)\\]\\[(.*)\\]()";
        part1 = message.replaceAll(pattern, "$1");
        part2 = message.replaceAll(pattern, "$2");

        //System.out.println("part1: " + part1 + " from OSC: " + m.getString(0));
        //System.out.println("part2: " + part2  + " from OSC: " + m.getString(0));
        this.d1 = new Definition(part1);
        this.d2 = new Definition(part2);
        this.bidirectional = m.typeTags().toCharArray()[3] == 'T';
        System.out.println("New Rule: " + m.textMessage());
    }

    /**
     * Check to see if the OSCMessage features in this translation
     *
     * @param m
     * @return
     */
    boolean contains(OSCMessage m) {
        if (m.translationSource() == this) {
            return false;
        }
        if (!bidirectional) {
            return d1.equals(m);
        } else {
            return (d1.equals(m) || d2.equals(m));
        }
    }

    /**
     * This has got to be very efficient
     *
     * @param m
     * @return
     */
    OSCMessage mediate(OSCMessage m) throws Exception {
        //System.out.println("d1: " + d1.textMessage() + " d2: " + d2.textMessage());
        OSCMessage y = null;

        if (d1.equals(m)) {
            y = d2.makeThisMessage(m, d1);
        } else if (d2.equals(m)) {
            y = d1.makeThisMessage(m, d2);
        }
        if (y != null) {
            y.translationSource(this);
            return y;
        }
        throw new Exception("Can't mediate that");
    }

    boolean isTheSameAs(Rule testr) {
        return (d1.get().equals(testr.d1.get())) && (d2.get().equals(testr.d2.get()));
    }
}
