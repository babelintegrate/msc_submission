package babel.Hub;

import static java.lang.Math.log10;
import static java.lang.Math.round;
import java.util.Arrays;

/**
 *
 * @author tom
 */
public class VariableMediationArgument extends MediationArgument {

    private final int width;
    private final double min, max;
    private final char type;
    private final boolean logScale;
    private final boolean linScale;

    private VariableMediationArgument() {
        super(null);
        index = "V0";
        min = 0;
        max = 0;
        width = 0;
        type = 0;
        logScale = false;
        linScale = false;
    }

    /**
     *
     * @param rule
     * @throws Exception
     */
    public VariableMediationArgument(String rule) throws Exception {
        super(rule);

        String rParts[] = super.rule.split(":");
        if (rParts.length != 5) {
            throw new Exception("Invalid argument in translation specification: " + Arrays.toString(rParts) + "\n\tRule: " + rule);
        }
        this.index = "V" + rParts[0];
        this.type = rParts[1].charAt(0);
        this.width = Integer.parseInt(rParts[2]);
        switch (rParts[3]) {
            case "LIN":
                this.linScale = true;
                this.logScale = false;
                break;
            case "LOG":
                this.linScale = false;
                this.logScale = true;
                break;
            case "F":
                this.linScale = false;
                this.logScale = false;
                break;
            default:
                System.err.println("ERROR IN TRANSLATION ARGUMENT: " + rule);
                this.linScale = false;
                this.logScale = false;
        }

        if (rParts[4].contains("--") || rParts[4].charAt(0) == '-') {
            System.err.println("NEGATIVE RANGES NOT YET SUPPORTED. Range set to 0-1");
            min = 0;
            max = 1;
        } else {
            String[] minMax = rParts[4].split("-");
            if (minMax[0].contains(".")) {
                this.min = Double.parseDouble(minMax[0]);
            } else {
                this.min = Integer.parseInt(minMax[0]);
            }
            if (minMax[1].contains(".")) {
                this.max = Double.parseDouble(minMax[1]);
            } else {
                this.max = Integer.parseInt(minMax[1]);
            }
        }
        //System.out.println("New argument: ID:" + this.index + " Type:" + this.type + " Range:" + min + "-" + max);
    }

    /**
     *
     * @return
     */
    public char type() {
        return type;
    }

    /**
     *
     * @return
     */
    public boolean scale() {
        return logScale || linScale;
    }

    /**
     *
     * @return
     */
    public boolean logScale() {
        return logScale;
    }

    /**
     *
     * @return
     */
    public boolean linScale() {
        return linScale;
    }
    
    /**
     * This is where the offset range checking and scaling happens.
     *
     * @param source
     * @param xDef
     * @return
     * @throws java.lang.Exception
     */
    public String value(String source, VariableMediationArgument xDef) throws Exception {
        //System.out.println("translating: " + i);
        double d = scale(source, xDef);
        switch (type) { //what type do I need?
            case 'i':
                value = "" + (int) round(d);
                break;
            case 'f':
                value = "" + (float) d;
                break;
        }
        //System.out.println("has become: " + value);
        if (width != 0) {
            value = leadZeros(value, width);
        }
        return value;
    }

    private String leadZeros(String toReplace, int width) {
        String temp = toReplace;
        while (temp.length() < width) {
            temp = "0" + temp;
        }
        //System.out.println("added zeros: " + temp + " to: " + toReplace);
        return temp;
    }

    //Think about this as the current file is the same type as i. You are converting to the correct type for matching arg
    private double scale(String i, VariableMediationArgument sourceArgument) throws Exception {
        double source = 0;
        switch (sourceArgument.type()) { //incoming type
            case 'i':
                //System.out.println("Trying to parse an int: " + i + " tempplate: " + sourceArgument.rule);
                source = Integer.parseInt(i);
                break;
            case 'f':
                source = Float.parseFloat(i);
                break;
            default:
                throw new Exception("Can't convert that type");
        }

        //System.out.println("Translating: " + source + " max: " + sourceArgument.max + " min: " + sourceArgument.min);
        if (source > sourceArgument.max || source < sourceArgument.min) {
            throw new Exception("Not a valid mediation, input is greater than expected range of " + sourceArgument.min + "-" + sourceArgument.max);
        }

        if (linScale && sourceArgument.linScale) {
            source = linToLin(source, sourceArgument.min, sourceArgument.max, min, max);
        } else if (linScale && sourceArgument.logScale) {
            source = linToLog(source, sourceArgument.min, sourceArgument.max, min, max);
        } else if (logScale && sourceArgument.logScale) {
            source = logToLog(source, sourceArgument.min, sourceArgument.max, min, max);
        } else if (logScale && sourceArgument.linScale) {
            source = logToLin(source, sourceArgument.min, sourceArgument.max, min, max);
        }

        /*if (source > max) {
            System.err.println("Translation has produced a value greater than the maximum allowed. ");
            source = max;
        } else if (source < min) {
            System.err.println("Translation has produced a value less than the minimum allowed.");
            source = min;
        }*/
        return source;
    }

    private boolean doneMaths = false;
    private double thisOffset;
    //private double linOffset;
    //private double linlin;
    private double linlog;
    private double loglin;
    private double loglog;

    private void doMaths(double a1, double b1, double a2, double b2) {
        if (doneMaths) {
            throw new Error("Allready done maths!");
        }
        thisOffset = pow(a2);

        //linlin = (double) (b2 - a2) / (b1 - a1);
        linlog = (pow(b2) - pow(a2)) / (b1 - a1);
        loglin = (b2 - a2) / (pow(b1 + a1) - pow(2 * a1));
        loglog = (pow(b2) - pow(a2)) / (pow(b1 + a1) - pow(2 * a1));

        //linOffset = a2 - (linlin * a1);

        doneMaths = true;
    }

    /**
     *
     * @param x = source
     * @param a1 = sourcemin
     * @param b1 = sopurcemax
     * @param a2 = min (thismin)
     * @param b2 = max (thismax)
     * @return
     */
    // x=127.5 a1=0 b1=255 a2=0 b2=1 expected 0.5 getting 127.5
    private double linToLin(double x, double a1, double b1, double a2, double b2) {
        if (!doneMaths) {
            doMaths(a1, b1, a2, b2);
        }
        if (x == a1) {
            return a2;
        } else if (x == b1) {
            return b2;
        }
        //return linOffset + (linlin * x);
        return (((double) (b2 - a2) / (double) (b1 - a1)) * (double) (x - a1)) + a2; //non optimised version is quicker! But incorrect???
    }

    private double linToLog(double x, double a1, double b1, double a2, double b2) {
        if (!doneMaths) {
            doMaths(a1, b1, a2, b2);
        }
        if (x == a1) {
            return a2;
        } else if (x == b1) {
            return b2;
        }
        return log(((x - a1) * linlog) + thisOffset);
    }

    private double logToLog(double x, double a1, double b1, double a2, double b2) {
        if (!doneMaths) {
            doMaths(a1, b1, a2, b2);
        }
        if (x == a1) {
            return a2;
        } else if (x == b1) {
            return b2;
        }
        return log((pow(x) * loglog) + thisOffset);
    }

    private double logToLin(double x, double a1, double b1, double a2, double b2) {
        if (!doneMaths) {
            doMaths(a1, b1, a2, b2);
        }
        if (x == a1) {
            return a2;
        } else if (x == b1) {
            return b2;
        }
        return (pow(x) * loglin) + a2;
    }

    private double pow(double d) {
        return Math.pow(10, d);
    }

    private double log(double d) {
        return log10(d);
    }

    String textMessage() {
        return rule;
    }

    //TESTS---------------------------------------------------------------------

    /**
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        //       ta1, ta2, test, expected , id
        test1("<1:i:0:LIN:0-9>", "<1:i:2:LIN:1-10>", "0", "01", 1.0);
        test1("<1:i:0:LIN:0-9>", "<1:i:2:LIN:1-10>", "9", "10", 1.1);

        test1("<1:i:0:LOG:0-9>", "<1:i:0:LIN:1-10>", "9", "10", 2.0);
        test1("<1:i:0:LOG:0-9>", "<1:i:0:LIN:1-10>", "0", "1", 2.1);

        test1("<1:i:0:LIN:0-9>", "<1:i:0:LOG:1-10>", "9", "10", 3.0);
        test1("<1:i:0:LIN:0-9>", "<1:i:0:LOG:1-10>", "0", "1", 3.1);
        
        test1("<1:i:0:LIN:0-9>", "<1:f:0:LOG:0-0.5>", "9", "0.5", 4.0);

        //LINLIN   x, a1, b1, a2, b2, aim, ID
        linlinTest(0, 0, 5, 1, 6, 1.0, 10.0);
        linlinTest(5, 0, 5, 1, 6, 6.0, 10.1);
        linlinTest(5, 0, 5, 0, 10, 10.0, 10.2);
        linlinTest(10, 0, 10, 0, 5, 5.0, 10.3);
        linlinTest(5, 0, 10, 0, 1, 0.5, 10.4);

        // x=127.5 a1=0 b1=255 a2=0 b2=1 expected 0.5 getting 127.5
        linlinTest(127.5, 0, 255, 0, 1, 0.5, 10.4);

        //LINLOG    ta, x, a1, b1, a2, b2, aim, ID
        linlogTest(0, 0, 5, 1, 6, 1.0, 11.0);
        linlogTest(5, 0, 5, 1, 6, 6.0, 11.1);

        //LOGLOG    ta, x, a1, b1, a2, b2, aim, ID
        loglogTest(0, 0, 5, 1, 6, 1.0, 12.0);
        loglogTest(5, 0, 5, 1, 6, 6.0, 12.1);

        //LOGLIN    ta, x, a1, b1, a2, b2, aim, ID
        logLinTest(0, 0, 5, 1, 6, 1.0, 13.0);
        logLinTest(5, 0, 5, 1, 6, 6.0, 14.1);
    }

    private static void test1(String sta1, String sta2, String testValue, String expected, double id) throws Exception {
        VariableMediationArgument ta1 = new VariableMediationArgument(sta1);
        VariableMediationArgument ta2 = new VariableMediationArgument(sta2);

        if (!ta2.value(testValue, ta1).equals(expected)) {
            System.err.println("Failed " + id + ": " + sta1 + "->" + sta2 + " Expected: " + expected + " got: " + ta2.value());
        } else {
            //System.out.println("Passed " + id);
        }
    }

    private static void linlinTest(double x, int a1, int b1, int a2, int b2, double aim, double id) {
        VariableMediationArgument ta = new VariableMediationArgument();
        if (ta.linToLin(x, a1, b1, a2, b2) == aim) {
            //System.out.println("passed test " + id);
        } else {
            System.err.println("Failed test " + id + " getting: " + ta.linToLin(x, a1, b1, a2, b2));
        }
    }

    private static void linlogTest(double x, int a1, int b1, int a2, int b2, double aim, double id) {
        VariableMediationArgument ta = new VariableMediationArgument();
        if (ta.linToLog(x, a1, b1, a2, b2) == aim) {
            //System.out.println("passed test " + id);
        } else {
            System.err.println("Failed test " + id + " getting: " + ta.linToLog(x, a1, b1, a2, b2));
        }
    }

    private static void loglogTest(double x, int a1, int b1, int a2, int b2, double aim, double id) {
        VariableMediationArgument ta = new VariableMediationArgument();
        if (ta.logToLog(x, a1, b1, a2, b2) == aim) {
            //System.out.println("passed test " + id);
        } else {
            System.err.println("Failed test " + id + " getting: " + ta.logToLog(x, a1, b1, a2, b2));
        }
    }

    private static void logLinTest(double x, int a1, int b1, int a2, int b2, double aim, double id) {
        VariableMediationArgument ta = new VariableMediationArgument();
        if (ta.logToLin(x, a1, b1, a2, b2) == aim) {
            //System.out.println("passed test " + id);
        } else {
            System.err.println("Failed test " + id + " getting: " + ta.logToLin(x, a1, b1, a2, b2));
        }
    }

    boolean checkBounds(String value) {
        double theValue = 0;
        switch (type){
            case 'i':
                theValue = (double) Integer.parseInt(value);
                break;
            case 'f':
                theValue = (double) Float.parseFloat(value);
                break;
        }
        return (theValue >= min && theValue <= max);
    }

    String min() {
        return "" + min;
    }

    String max() {
        return "" + max;
    }
}
