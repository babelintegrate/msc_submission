package babel.Hub;

import babel.Babel;
import babel.OSC.OSCMessage;
import babel.TMS.TMS;
import babel.TextReader.TextReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * The Hub uses a TMS to recieve messages from threads. It then sorts through
 * messages and places them in buffers for each destination
 *
 * @author tom
 */
public final class Hub extends Thread {

    private static final Hub HUB = new Hub();
    private final ArrayList<TMS> modules = new ArrayList<>();
    private final TMS MESSAGES = new TMS();
    private final AtomicBoolean running = new AtomicBoolean(false);
    private final ArrayList<Rule> rules = new ArrayList<>();
    private final ArrayList<MessageTimer> messageTimers = new ArrayList<>();
    private Babel babel;

    private Hub() {
        super("Hub");
    }

    /**
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        TextReader tr = TextReader.get();
        OSCMessage m = tr.read("/system/rule,ssT,[/dmx/0/<1:i:0:F:0-31>,i,<2:i:0:LIN:0-255>]|[/ch/<1:i:0:F:1-32>/mix/fader,f,<2:f:0:LIN:0-1>]");
        HUB.messageProcessor(m);
        m = tr.read("/dmx/0/2,i,127"); //should churn out an OSC message /ch
        HUB.messageProcessor(m);
    }

    /**
     * get the Hub instance
     *
     * @return
     */
    public static Hub get() {
        return HUB;
    }

    /**
     * Use this to add a message to the Hub
     *
     * @param m
     */
    public void add(OSCMessage m) {
        MESSAGES.add(m);
    }

    /**
     * Start the Hub
     */
    @Override
    public void run() {
        running.set(true);
        while (running.get()) {
            jobs();
            sleep();
        }
    }

    /**
     * Jobs to do. Keeps run method short
     */
    private void jobs() {
        if (MESSAGES.check()) {
            try {
                OSCMessage m = MESSAGES.get();
                //System.out.println("Message in Hub: " + m.textMessage()); //MAKES EVERYTHING SLOW
                messageProcessor(m);
            } catch (Exception ex) {
                System.err.println("Error in message: " + ex.getLocalizedMessage());
                System.err.println("" + ex.toString());
                MESSAGES.bin();
            }
        }
    }

    /**
     * THIS HAS GOT TO BE SUPER EFFICIENT
     *
     * @param m
     */
    private void messageProcessor(OSCMessage m) {

        if (m.addressPattern().startsWith("/system/")) {
            systemMessage(m);
            return;
        }

        //Normal messages
        try {
            mediate(m);
        } catch (Exception ex) {
            //System.err.println("HUB: Translation Failed: " + ex.toString());
        }

        for (TMS module : modules) {
            module.add(m);
        }
    }

    private void systemMessage(OSCMessage m) {
        //Catch textmessages
        if (m.addressPattern().startsWith("/system/textmessage")) {
            parseTextMessage(m);
        } else if (m.addressPattern().startsWith("/system/rule")) {
            decodeNewRule(m);
        } else if (m.addressPattern().startsWith("/system/control/")) {
            controlMessage(m.addressPattern());
        } else if (m.addressPattern().startsWith("/system/messagetimer")) {
            try {
                messageTimers.add(new MessageTimer(m));
            } catch (Exception ex) {
                System.err.println("Cannot add MessageTimer: " + m.textMessage());
            }
        } else {
            for (TMS module : modules) {
                module.add(m);
            }
        }
    }

    private void controlMessage(String m) {
        switch (m) {
            case "/system/control/restart":
                System.out.println("Restarting");
                babel.shutdown(2);
                break;
            case "/system/control/longrestart":
                System.out.println("Restarting");
                babel.shutdown(20);
                break;
            case "/system/control/update":
                System.out.println("Updating");
                babel.shutdown(3);
                break;
            case "/system/control/shutdown":
                System.out.println("Shuttingdown");
                babel.shutdown(4);
                break;
            case "/system/control/reboot":
                System.out.println("Rebooting");
                babel.shutdown(5);
                break;
        }
    }

    //private final Timer t = new Timer();
    private void mediate(OSCMessage m) throws Exception {
        for (Rule r : rules) {
            if (r.contains(m)) {
                try{
                    //t.timeStart();
                    add(r.mediate(m));
                    //t.timeStop();
                } catch (Exception ex){
                   
                }
            }
        }
    }

    private void parseTextMessage(OSCMessage m) {
        if (!m.typeTags().equals(",s")) {
            return;
        }
        TextReader tr = TextReader.get();
        try {
            add(tr.read(m.getString(0)));
        } catch (Exception ex) {
            System.err.println("Could not parse text message: " + ex.getMessage());
        }
    }

    private void decodeNewRule(OSCMessage m) {

        try {
            /*Rule r = new Rule(m); // causes problems for rules that have the same ap and tt but ifferent ranges
            for (Rule testr : rules) {
                if (r.isTheSameAs(testr));
                return; //rule already exists
            }*/
            rules.add(new Rule(m));
        } catch (Exception ex) {
            System.err.println("Error decoding mediation rule" + m.textMessage() + " : " + ex.toString());
        }
    }

    private void sleep() {
        try {
            Thread.sleep(0, 1);
        } catch (InterruptedException e) {
            System.out.println("Exception in Pinger");
        }
    }

    /**
     *
     * @return
     */
    public TMS register() {
        TMS t = new TMS();
        modules.add(t);
        return t;
    }

    /**
     *
     */
    public void shutdown() {
        running.set(false);
        for (MessageTimer m : messageTimers) {
            m.shutdown();
        }
    }

    private void runSystemCommand(String command) throws Exception {
        int i = 100;

        try {
            Process p = Runtime.getRuntime().exec(command);
            i = p.waitFor(); //Get exit status
        } catch (IOException | InterruptedException ex) {
            System.out.println("System Command Exception. " + ex.getLocalizedMessage());
        }
    }

    /**
     *
     * @param b
     */
    public void babel(Babel b) {
        babel = b;
    }
}
