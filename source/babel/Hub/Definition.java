package babel.Hub;

import babel.OSC.OSCMessage;
import babel.TextReader.TextReader;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author tom
 */
public class Definition {

    private final String rule;
    private final String tt, textMessage;
    private final ArrayList<Integer> locationOfArgVariables = new ArrayList<>();
    private final ArrayList<Integer> locationOfArgConstants = new ArrayList<>();
    private final String[] ap, args;
    private final ArrayList<Argument> arguments = new ArrayList<>();

    enum Defines {
        AP("AP"), ARG("ARG"),
        Const("CONST"), VAR("VAR");

        private final String value;

        private Defines(String value) {
            this.value = value;
        }
    }

    class Argument {

        private final Defines place;
        private final Defines type;
        private final int location;
        private final MediationArgument ma;

        public Argument(Defines place, Defines type, int location, MediationArgument ma) {
            this.place = place;
            this.type = type;
            this.location = location;
            this.ma = ma;
        }
    }

    /**
     * Split textmessage on , first. Store tt Split the ap part on / and store
     * as apParts Split the args on | and store as argsParts
     *
     * Find any apPart featuring {@literal <} create an argument from it. Replace apPart
     * with {@literal <index>}
     *
     * Do the same for args
     *
     * @param rule
     * @throws java.lang.Exception
     */
    public Definition(String rule) throws Exception {
        this.rule = rule;
        String[] parts = rule.split(",");
        this.tt = parts[1];
        this.ap = parts[0].split("/");
        this.args = parts[2].split("\\|");

        for (int i = 0; i < ap.length; i++) {
            if (ap[i].contains("<")) {
                //System.out.println("got an arg. Location: " + i + " part: " + Arrays.toString(apParts));
                VariableMediationArgument arg = new VariableMediationArgument(ap[i]);
                arguments.add(new Argument(Defines.AP, Defines.VAR, i, arg));
                ap[i] = "<" + arg.index() + ">";
            }
        }

        for (int i = 0; i < args.length; i++) {
            if (args[i].contains("<")) {
                //System.out.println("got an arg. Location: " + i + " part: " + Arrays.toString(argsParts));
                VariableMediationArgument arg = new VariableMediationArgument(args[i]);
                locationOfArgVariables.add(i);
                arguments.add(new Argument(Defines.ARG, Defines.VAR, i, arg));
                args[i] = "<" + arg.index() + ">";
            } else {
                //System.out.println("go a const: " + argsParts[i] + " parts: " + Arrays.toString(argsParts));
                ConstantMediationArgument arg = new ConstantMediationArgument(args[i], "" + i);
                locationOfArgConstants.add(i);
                arguments.add(new Argument(Defines.ARG, Defines.Const, i, arg));
                args[i] = "<" + arg.index + ">";
            }
        }

        //Build the textMessage
        String temp = "";
        for (int i = 1; i < ap.length; i++) {
            temp += "/" + ap[i];
        }

        temp += "," + tt + ",";
        for (int i = 0; i < args.length; i++) {
            if (i != 0) {
                temp += "|";
            }
            temp += args[i];
        }
        this.textMessage = temp;
        //System.out.println("textMessage: " + textMessage);

        //System.out.println(Arrays.toString(apParts) + tt + Arrays.toString(argsParts));
    }

    boolean check(String m) { //THIS HAS TO BE SUPER EFFICIENT
        return m.equals(rule);
    }

    /**
     * This has got to be very efficient
     *
     * @param m
     * @return
     */
    boolean equals(OSCMessage m) {

        //System.out.println("Testing to see if: " + m.textMessage() + " = this: " + textMessage);
        if (!apEquals(m)) {
            //System.out.println("Ap is different");
            return false;
        }
        if (!ttEquals(m)) {
            //System.out.println("tt is different");
            return false;
        }
        return argsEquals(m);
    }

    private boolean apEquals(OSCMessage m) {
        String[] m_apParts = m.addressPattern().split("/");

        if (m_apParts.length != ap.length) {
            //System.out.println("AP lengths");
            return false;
        }

        //is AP the same? Ignoring and variables!
        for (int i = 1; i < ap.length; i++) {
            if (ap[i].charAt(0) != '<') {
                if (!ap[i].equals(m_apParts[i])) {
                    //System.out.println("AP don't match");
                    return false;
                }
            }
        }

        return true;
    }

    private boolean ttEquals(OSCMessage m) {
        //Are the tts the same? Ignoring any variables!
        char[] ttParts = tt.toCharArray(); //Should do this in constructor to keep speed?
        char[] m_ttParts = m.typeTagsNoComma().toCharArray();
        if (m_ttParts.length != ttParts.length) {
            System.out.println("TT not same length: " + Arrays.toString(m_ttParts) + Arrays.toString(ttParts));
            return false;
        }

        for (int i = 1; i < ttParts.length; i++) {
            if (ttParts[i] != 'T' || ttParts[i] != 'F') { //if == b then throuw error - not yet supported
                if (!(ttParts[i] == m_ttParts[i])) {
                    //System.out.println("TT don't match");
                    return false;
                }
            }
        }

        return true;
    }

    private boolean argsEquals(OSCMessage m) { //Check valid ranges and consts

        int expectedNoOfArgs = locationOfArgConstants.size() + locationOfArgVariables.size();

        String[] xParts = m.textMessage().split(",");
        String[] xArgs = xParts[2].split("\\|");
        if (xArgs.length != expectedNoOfArgs) {
            //System.out.println("Arg lengths are different");
            return false;
        }
        for (Argument ArgWithLoc : arguments) {
            if (ArgWithLoc.place == Defines.ARG && ArgWithLoc.type == Defines.Const) {
                ConstantMediationArgument cma = (ConstantMediationArgument) ArgWithLoc.ma;
                if (!xArgs[ArgWithLoc.location].equals(cma.value)) {
                    //System.out.println("Arg is wrong value. Should be: " + cma.value + " is: " + xArgs[ArgWithLoc.location]);
                    return false;
                }
            } else if (ArgWithLoc.place == Defines.ARG && ArgWithLoc.type == Defines.VAR) {
                VariableMediationArgument vma = (VariableMediationArgument) ArgWithLoc.ma;
                String value = xArgs[ArgWithLoc.location];
                if (!vma.checkBounds(value)) {
                    //System.out.println("Arg is wrong value. Should be within bounds: " + vma.min() + "-" + vma.max() + " is: " + Float.parseFloat(value));
                    return false;
                }
            }
        }
        return true;
    }

    OSCMessage makeThisMessage(OSCMessage x, Definition xDefinition) throws Exception {
        //System.out.println("Source: " + source.textMessage() + " Template: " + sourceDefinition.textMessage + " Destination: " + textMessage);
        String xString = x.textMessage();
        String[] xParts = xString.split(",");
        String[] xAP = xParts[0].split("/");
        String[] xArgs = xParts[2].split("\\|");

        //System.out.println("x   : " + x.textMessage() + " \nxDef: " + xDefinition.textMessage() + "\nyDef: " + textMessage() + "\n");
        //Extract the arguments from x using xDefinition------------------------        
        // And compute the values for the new message (y)
        for (Argument a : xDefinition.arguments) {
            switch (a.place) {
                case AP:
                    if (a.type == Defines.VAR) {
                        computeValue(xAP[a.location], (VariableMediationArgument) a.ma);
                    }
                    break;
                case ARG:
                    if (a.type == Defines.VAR) {
                        computeValue(xArgs[a.location], (VariableMediationArgument) a.ma);
                    }
                    break;
            }
        }
        //----------------------------------------------------------------------
        //Build Y---------------------------------------------------------------
        String y = textMessage;
        //System.out.println("Starting to build");
        //System.out.println("\ta) " + y);

        for (Argument a : arguments) {
            //System.out.println("xArg: " + xArg.value);
            switch (a.type) {
                case Const:
                    ConstantMediationArgument cArg = (ConstantMediationArgument) a.ma;
                    y = y.replaceAll("<" + cArg.index() + ">", cArg.value);
                    break;
                case VAR:
                    VariableMediationArgument vArg = (VariableMediationArgument) a.ma;
                    y = y.replaceAll("<" + vArg.index() + ">", vArg.value);
                    break;
            }
        }
        //System.out.println("\tb) " + y);
        //----------------------------------------------------------------------

        //System.out.println("");
        TextReader tr = new TextReader();
        return tr.read(y);
        //return new OSCMessage(this, "/blank", Type.Null);
    }

    private void computeValue(String xVal, VariableMediationArgument xDefinition) {
        //System.out.println("Computing value for: " + xVal + " using definition: " + xDefinition.rule);

        for (Argument a : arguments) {
            if (a.ma.index.equals(xDefinition.index)) {
                try {
                    VariableMediationArgument va = (VariableMediationArgument) a.ma;
                    va.value(xVal, xDefinition);
                    //System.out.println("converted to: " + ma.value(xVal, xDefinition));
                } catch (Exception ex) {
                    System.err.println("Can't convert value: " + ex.getLocalizedMessage());
                }
            }
        }
    }

    String textMessage() {
        return textMessage;
    }

    String get() {
        return rule;
    }

    /**
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        String d1, d2, m, aim;

        d1 = "/dmx/0/<1:i:0:LIN:0-31>,i,<2:i:0:LIN:0-255>";
        d2 = "/ch/<1:i:0:LIN:1-32>/mix/fader,f,<2:f:0:LIN:0-1>";
        m = "/dmx/0/0,i,128";
        aim = "/ch/1/mix/fader,f,0.5019608";
        test(d1, d2, m, aim, 1.0);

        m = "/ch/32/mix/fader,f,0.5";
        aim = "/dmx/0/31,i,128";
        test(d1, d2, m, aim, 1.1);

        d1 = "/midi/0/176,ii,<1:i:0:F:0-8>|<2:i:0:LIN:0-127>";
        d2 = "/ch/<1:i:0:F:0-8>/mix/fader,f,<2:f:0:LIN:0-1>";
        m = "/midi/0/176,ii,1|127";
        aim = "/ch/1/mix/fader,f,1.0";
        test(d1, d2, m, aim, 2.0);

        d1 = "/midi/0/176,ii,0|<2:i:0:LIN:0-127>";
        d2 = "/ch/02/mix/fader,f,<2:f:0:LIN:0-1>";
        m = "/midi/0/176,ii,0|127";
        aim = "/ch/02/mix/fader,f,1.0";
        test(d1, d2, m, aim, 3.0);

        m = "/midi/0/176,ii,1|127";
        testToFail(d1, d2, m, aim, 3.1);

        d1 = "/midi/0/176,ii,9|127";
        d2 = "/fx/3/par/02,f,1.0";
        m = "/midi/0/176,ii,9|127";
        aim = "/fx/3/par/02,f,1.0";
        test(d1, d2, m, aim, 4.0);

    }

    /**
     *
     * @param stringd1
     * @param stringd2
     * @param testMessage
     * @param aim
     * @param id
     * @throws Exception
     */
    public static void test(String stringd1, String stringd2, String testMessage, String aim, double id) throws Exception {
        String y = "";
        TextReader tr = TextReader.get();
        Definition x1 = new Definition(stringd1);
        Definition x2 = new Definition(stringd2);
        OSCMessage m = tr.read(testMessage);
        if (x1.equals(m)) {
            y = x2.makeThisMessage(m, x1).textMessage();
        } else if (x2.equals(m)) {
            y = x1.makeThisMessage(m, x2).textMessage();
        }
        if (!y.equals(aim)) {
            System.out.println(id + " failed. \n\t" + m.textMessage() + " gave: " + y + "\n");
            throw new Error("Test Failed");
        }
    }

    /**
     *
     * @param stringd1
     * @param stringd2
     * @param testMessage
     * @param aim
     * @param id
     * @throws Exception
     */
    public static void testToFail(String stringd1, String stringd2, String testMessage, String aim, double id) throws Exception {
        String y = "";
        TextReader tr = TextReader.get();
        Definition x1 = new Definition(stringd1);
        Definition x2 = new Definition(stringd2);
        OSCMessage m = tr.read(testMessage);

        if (x1.equals(m)) {
            y = x2.makeThisMessage(m, x1).textMessage();
        } else if (x2.equals(m)) {
            y = x1.makeThisMessage(m, x2).textMessage();
        }
        if (!y.isEmpty()) {
            System.out.println(id + " failed. Should have given nothing.\n\t" + m.textMessage() + " gave: " + y + "\n");
            throw new Error("Test Failed");
        }

    }
}
