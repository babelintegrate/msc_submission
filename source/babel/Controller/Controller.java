package babel.Controller;

import babel.Hub.Hub;
import babel.OSC.OSCMessage;
import babel.OSC.OSCMessageBuilder;
import babel.OSC.OSCtcpServer;
import babel.TMS.TMS;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 *
 * @author T R Bland
 */
public class Controller extends Thread {

    private final AtomicBoolean running = new AtomicBoolean(false);
    private final OSCtcpServer controlServer;
    private final Hub hub = Hub.get();
    private final TMS messagesFromHub;

    /**
     *
     */
    public Controller() {
        super("Controller");
        controlServer = new OSCtcpServer(false, false, 2323); //messagesToHub, Faedback, Port
        messagesFromHub = hub.register();
    }

    @Override
    public void run() {
        controlServer.start();
        running.set(true);
        while (running.get()) {
            checkHub();
            checkForServerMessages();
            sleep();
        }
    }

    private void sleep() {
        try {
            Thread.sleep(0,1);
        } catch (InterruptedException e) {
            System.out.println("Exception in Pinger");
        }
    }

    private void checkHub() {
        if (messagesFromHub.check()) {
            try {
                decode(messagesFromHub.get());
            } catch (Exception ex) {
                System.out.println("Controller checking for controlMessages: " + ex.getLocalizedMessage());
            }
        }
    }

    private void checkForServerMessages() {
        if (controlServer.messages.check()) {
            try {
                OSCMessage message = controlServer.messages.get();
                /*if (message != null) {
                    System.out.println("\033[36m" + new java.util.Date() + " - Controller RX: " + message.addressPattern() + message.typeTags() + " " + Arrays.toString(message.getAllData()) + "\033[0m");
                }*/
                Hub.get().add(message);
            } catch (Exception ex) {
                System.err.println("Controller.checkForServerMessages: new message: COULD NOT GET MESSAGE " + ex.toString());
            }
        }
    }

    private void decode(OSCMessage m) {
        controlServer.send(m);
    }
    
    /**
     *
     */
    public void shutdown(){
        running.set(false);
        OSCMessageBuilder mb = new OSCMessageBuilder(this, "/system/control/shuttingdown");
        mb.addNull();
        OSCMessage m = mb.finalise();
        controlServer.send(m);
        System.out.println("SENDING: " + Arrays.toString(m.stream()));
        
        controlServer.shutdown();
    }
}
